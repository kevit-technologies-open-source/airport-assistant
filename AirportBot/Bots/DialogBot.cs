﻿// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT License.

using Microsoft.Bot.Builder;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Builder.Dialogs.Choices;
using Microsoft.Bot.Schema;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace Microsoft.BotBuilderSamples
{
    // This IBot implementation can run any type of Dialog. The use of type parameterization is to allows multiple different bots
    // to be run at different endpoints within the same project. This can be achieved by defining distinct Controller types
    // each with dependency on distinct IBot types, this way ASP Dependency Injection can glue everything together without ambiguity.
    // The ConversationState is used by the Dialog system. The UserState isn't, however, it might have been used in a Dialog implementation,
    // and the requirement is that all BotState objects are saved at the end of a turn.
    public class DialogBot<T> : ActivityHandler where T : ComponentDialog
    {
        protected readonly BotState ConversationState;
        protected readonly Dialog Dialog;
        protected readonly ILogger Logger;
        protected readonly BotState UserState;
        protected readonly DialogContext dialogContext;

        const string Yes = "Yes";
        const string No = "No";
        const string GoTerminal3 = "Yes Terminal 3";
        const string GoToMain = "No Terminal 3";

        public DialogBot(ConversationState conversationState, UserState userState, T dialog, ILogger<DialogBot<T>> logger)
        {
            ConversationState = conversationState;
            UserState = userState;
            Dialog = dialog;
            Logger = logger;
        }

        public override async Task OnTurnAsync(ITurnContext turnContext, CancellationToken cancellationToken = default(CancellationToken))
        {
            var activity = turnContext.Activity;
            if (string.IsNullOrWhiteSpace(activity.Text) && activity.Value != null)
            {
                activity.Text = JsonConvert.SerializeObject(activity.Value);

                if (activity.Text.Contains("WebCheckFormSubmit"))
                {
                    JToken commandToken = JToken.Parse(turnContext.Activity.Value.ToString());
                    string FullName = commandToken["FullName"].Value<string>();
                    string DOB = commandToken["DOB"].Value<string>();
                    string PassportNumber = commandToken["PassportNumber"].Value<string>();
                    if (string.IsNullOrEmpty(FullName) || string.IsNullOrEmpty(DOB) || string.IsNullOrEmpty(PassportNumber))
                    {
                        await turnContext.SendActivityAsync("Please fill all fields", cancellationToken: cancellationToken);
                        //await base.OnTurnAsync(turnContext, cancellationToken);
                    }
                    else
                    {
                        var message = $"This is perfect, I have verified all your details and your web check in has been successful! \U0001F600 {Environment.NewLine} Looks like we're done, here's a tip, since you're flying international expect your flight to arrive at the terminal 3";
                        await turnContext.SendActivityAsync(message, cancellationToken: cancellationToken);
                        var choices = new List<Choice>();
                        choices.Add(new Choice() { Value = GoTerminal3, Action = new CardAction() { DisplayText = Yes, Title = Yes, Text = Yes, Type = ActionTypes.ImBack, Value = GoTerminal3 } });
                        choices.Add(new Choice() { Value = GoTerminal3, Action = new CardAction() { DisplayText = No, Title = No, Text = No, Type = ActionTypes.ImBack, Value = GoToMain } });
                        var Mapmessage = ChoiceFactory.ForChannel(turnContext.Activity.ChannelId, choices, $"Would you like to view a guide to T3? ");
                        await turnContext.SendActivityAsync(Mapmessage, cancellationToken);
                    }
                }
            }
            await base.OnTurnAsync(turnContext, cancellationToken);
            // Save any state changes that might have occured during the turn.
            await ConversationState.SaveChangesAsync(turnContext, true, cancellationToken);
            await UserState.SaveChangesAsync(turnContext, true, cancellationToken);
        }

        protected override async Task OnMessageActivityAsync(ITurnContext<IMessageActivity> turnContext, CancellationToken cancellationToken)
        {
            Logger.LogInformation("Running dialog with Message Activity.");
            // Run the Dialog with the new message Activity.
            await Dialog.RunAsync(turnContext, ConversationState.CreateProperty<DialogState>(nameof(DialogState)), cancellationToken);
        }

        private Attachment CreateAdaptiveCardAttachment()
        {
            var cardResourcePath = "CoreBot.Cards.welcomeCard.json";

            using (var stream = GetType().Assembly.GetManifestResourceStream(cardResourcePath))
            {
                using (var reader = new StreamReader(stream))
                {
                    var adaptiveCard = reader.ReadToEnd();
                    return new Attachment()
                    {
                        ContentType = "application/vnd.microsoft.card.adaptive",
                        Content = JsonConvert.DeserializeObject(adaptiveCard),
                    };
                }
            }
        }
    }
}
