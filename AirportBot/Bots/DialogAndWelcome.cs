﻿// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT License.

using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Bot.Builder;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Builder.Dialogs.Choices;
using Microsoft.Bot.Schema;
using Microsoft.Extensions.Logging;

namespace Microsoft.BotBuilderSamples
{
    public class DialogAndWelcomeBot<T> : DialogBot<T> where T : ComponentDialog
    {
        const string ViewFlightsOption = "View Flights";
        const string WebCheckInOption = "Web Check In";
        const string AirportTourInOption = "Airport Tour";
        const string FAQInOption = "FAQ";

        public DialogAndWelcomeBot(ConversationState conversationState, UserState userState, T dialog, ILogger<DialogBot<T>> logger)
            : base(conversationState, userState, dialog, logger)
        {
        }

        protected override async Task OnMembersAddedAsync(
            IList<ChannelAccount> membersAdded,
            ITurnContext<IConversationUpdateActivity> turnContext,
            CancellationToken cancellationToken)
        {
            foreach (var member in membersAdded)
            {
                // Greet anyone that was not the target (recipient) of this message.
                // To learn more about Adaptive Cards, see https://aka.ms/msbot-adaptivecards for more details.
                if (member.Id != turnContext.Activity.Recipient.Id)
                {
                    var choices = new List<Choice>();
                    choices.Add(new Choice() { Value = ViewFlightsOption, Action = new CardAction() { Title = ViewFlightsOption, Type = ActionTypes.ImBack, Value = ViewFlightsOption } });
                    choices.Add(new Choice() { Value = WebCheckInOption, Action = new CardAction() { Title = WebCheckInOption, Type = ActionTypes.ImBack, Value = WebCheckInOption } });
                    choices.Add(new Choice() { Value = AirportTourInOption, Action = new CardAction() { Title = AirportTourInOption, Type = ActionTypes.ImBack, Value = AirportTourInOption } });
                    choices.Add(new Choice() { Value = FAQInOption, Action = new CardAction() { Title = FAQInOption, Type = ActionTypes.ImBack, Value = FAQInOption } });
                    var message = ChoiceFactory.ForChannel(turnContext.Activity.ChannelId, choices, $"Welcome to the Delhi Airpot! I am your personal virtual assistant in your service.{Environment.NewLine} Please tell me, what may I assist you with? {Environment.NewLine} You may type your message or press on the mic to ask me for assistance.");
                    await turnContext.SendActivityAsync(message, cancellationToken);
                }
            }
        }
    }
}