﻿using Microsoft.Bot.Builder;
using Microsoft.Bot.Builder.AI.Luis;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace AirportBot
{
    public class AirportBotRecognizer : IRecognizer
    {
        private readonly LuisRecognizer _recognizer;

        public AirportBotRecognizer(IConfiguration configuration)
        {
            var luisApplication = new LuisApplication(
                "83369cc8-075c-425d-bc79-1e76f342693a",
                "a6b67f3a60f24c7b8e6e3cf3825bd290",
                "https://westus.api.cognitive.microsoft.com/luis/api/v2.0");
            _recognizer = new LuisRecognizer(luisApplication);
        }

        // Returns true if luis is configured in the appsettings.json and initialized.
        public virtual bool IsConfigured => _recognizer != null;

        public virtual async Task<RecognizerResult> RecognizeAsync(ITurnContext turnContext, CancellationToken cancellationToken)
            => await _recognizer.RecognizeAsync(turnContext, cancellationToken);

        public virtual async Task<T> RecognizeAsync<T>(ITurnContext turnContext, CancellationToken cancellationToken)
            where T : IRecognizerConvert, new()
            => await _recognizer.RecognizeAsync<T>(turnContext, cancellationToken);
    }
}
