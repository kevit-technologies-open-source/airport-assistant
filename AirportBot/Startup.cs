// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT License.
//
// Generated with Bot Builder V4 SDK Template for Visual Studio EmptyBot v4.6.2

using AirPortBot;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Bot.Builder;
using Microsoft.Bot.Builder.AI.Luis;
using Microsoft.Bot.Builder.Integration.AspNet.Core;
using Microsoft.BotBuilderSamples;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace AirportBot
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
           

            // Create and register a LUIS recognizer.

            //For AirportBot In LUIS
            //services.AddSingleton(sp =>
            //{
            //    // Set up Luis
            //    var luisApp = new LuisApplication(
            //        applicationId: "83369cc8-075c-425d-bc79-1e76f342693a",
            //        endpointKey: "a6b67f3a60f24c7b8e6e3cf3825bd290",
            //        endpoint: "https://westus.api.cognitive.microsoft.com/");
            //    // Specify LUIS options. These may vary for your bot.
            //    var luisPredictionOptions = new LuisPredictionOptions
            //    {
            //        IncludeAllIntents = true,
            //    };
            //    return new LuisRecognizer(
            //        application: luisApp,
            //        predictionOptions: luisPredictionOptions,
            //        includeApiResults: true);
            //});



            //applicationId  = Application ID
            //endpointKey = Primary key
            //endpoint = Endpoint Url:


            //For AirportBot In LUIS
            services.AddSingleton(sp =>
            {
                // Set up Luis
                var luisApp = new LuisApplication(
                    applicationId: "8e34929a-41c3-4710-a5bb-0f331a065d6b",
                    endpointKey: "a6b67f3a60f24c7b8e6e3cf3825bd290",
                    endpoint: "https://westus.api.cognitive.microsoft.com/");
                // Specify LUIS options. These may vary for your bot.
                var luisPredictionOptions = new LuisPredictionOptions
                {
                    IncludeAllIntents = true,
                };
                return new LuisRecognizer(
                    application: luisApp,
                    predictionOptions: luisPredictionOptions,
                    includeApiResults: true);
            });

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            // Create the Bot Framework Adapter with error handling enabled.
            services.AddSingleton<IBotFrameworkHttpAdapter, AdapterWithErrorHandler>();

            services.AddSingleton<IStorage, MemoryStorage>();

            // Create the User state. (Used in this bot's Dialog implementation.)
            services.AddSingleton<UserState>();

            // Create the Conversation state. (Used by the Dialog system itself.)
            services.AddSingleton<ConversationState>();

            services.AddSingleton<MainDialog>();

            // Create the bot as a transient. In this case the ASP Controller is expecting an IBot.
            services.AddTransient<IBot, DialogAndWelcomeBot<MainDialog>>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }
            app.UseDefaultFiles();
            app.UseStaticFiles();

            app.UseWebSockets();
            app.UseMvc();
        }
    }
}
