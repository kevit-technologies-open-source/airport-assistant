﻿using AirportBot.Dialog;
using Microsoft.Bot.Builder;
using Microsoft.Bot.Builder.AI.Luis;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Builder.Dialogs.Choices;
using Microsoft.Bot.Schema;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace AirPortBot.Dialog
{
    public class WebCheckInDialog : ComponentDialog
    {
        private LuisRecognizer Recognizer { get; } = null;

        public WebCheckInDialog(LuisRecognizer luis)
            : base(nameof(WebCheckInDialog))
        {
            this.Recognizer = luis ?? throw new System.ArgumentNullException(nameof(luis));
            AddDialog(new WaterfallDialog(nameof(WaterfallDialog), new WaterfallStep[]
           {
                AskPNRNumberStepAsync,
                WebCheckInStatusStepAsync,
                WebCheckInProcessStepAsync,
                FormSelectionPreferenceResult
           }));

            AddDialog(new TextPrompt(nameof(TextPrompt)));
            AddDialog(new ChoicePrompt(nameof(ChoicePrompt)));
            AddDialog(new WebCheckInFormDialog(luis));
            AddDialog(new WebCheckInVoiceFormDialog(luis));
            AddDialog(new DefaultDialog(luis));
           // AddDialog(new LUISDefaultDialog(luis));

            InitialDialogId = nameof(WaterfallDialog);
        }

        private static async Task<DialogTurnResult> AskPNRNumberStepAsync(WaterfallStepContext stepContext, CancellationToken cancellationToken)
        {
            return await stepContext.PromptAsync(nameof(TextPrompt), new PromptOptions { Prompt = MessageFactory.Text($"Of course! I see that you are ready to check in, please enter your PNR number and I'll make this quick for you", InputHints.ExpectingInput) }, cancellationToken);
        }

        private static async Task<DialogTurnResult> WebCheckInStatusStepAsync(WaterfallStepContext stepContext, CancellationToken cancellationToken)
        {
            var PNRNumber = stepContext.Result;
            if (PNRNumber != null)
            {
                await stepContext.Context.SendActivityAsync(MessageFactory.Text($"Perfect, here are your booking details"), cancellationToken: cancellationToken);

                var attachments = new List<Attachment>();
                var reply = MessageFactory.Attachment(attachments);
                reply.Attachments.Add(Common.CreateAdaptiveCardAttachment(Path.Combine(".", "Resources", "FlightItineraryCard.json")));
                await stepContext.Context.SendActivityAsync(reply, cancellationToken);

                return await stepContext.PromptAsync(nameof(ChoicePrompt),
                new PromptOptions
                {
                    Prompt = MessageFactory.Text("Would you like to proceed towards completing your web check in process ?", InputHints.ExpectingInput),
                    RetryPrompt = MessageFactory.Text("That was not a valid choice, please select from given options."),
                    Choices = new List<Choice>
                   {
                       new Choice
                       {
                            Value = "Sure",
                            Synonyms = new List<string>
                            {
                                "Sure",
                                "sure",
                                "yes",
                                "Yes",
                                "YES"
                            },
                       },
                       new Choice
                       {
                            Value = "No",
                            Synonyms = new List<string>
                            {
                                "No",
                                "no",
                                "NO"
                            },
                       }
                   },
                }, cancellationToken);
            }
            else
            {
                return await stepContext.PromptAsync(nameof(TextPrompt), new PromptOptions { Prompt = MessageFactory.Text($"Please enter valid PNR number", InputHints.ExpectingInput) }, cancellationToken);
            }
        }

        private async Task<DialogTurnResult> WebCheckInProcessStepAsync(WaterfallStepContext stepContext, CancellationToken cancellationToken)
        {
            var SelectedOtherOption = ((FoundChoice)stepContext.Result).Value;
            if (SelectedOtherOption.ToLower().Equals("sure"))
            {
                return await stepContext.PromptAsync(nameof(ChoicePrompt),
                 new PromptOptions
                 {
                     Prompt = MessageFactory.Text("Sure, I will just need you to fill in a few details now", InputHints.ExpectingInput),
                     RetryPrompt = MessageFactory.Text("That was not a valid choice, please select from given option."),
                     Choices = ChoiceFactory.ToChoices(new List<string> { "View Form", "Fill with Voice" }),
                 }, cancellationToken);
            }
            else if (SelectedOtherOption.ToLower().Equals("no"))
            {
                await stepContext.EndDialogAsync(null, cancellationToken);
                return await stepContext.BeginDialogAsync(nameof(DefaultMainDialog), null, cancellationToken);
            }
            else
            {
                await stepContext.PromptAsync(nameof(TextPrompt), new PromptOptions { Prompt = MessageFactory.Text($"Invalid Choice") }, cancellationToken);
                return await stepContext.EndDialogAsync(null, cancellationToken);
            }
        }

        private async Task<DialogTurnResult> FormSelectionPreferenceResult(WaterfallStepContext stepContext, CancellationToken cancellationToken)
        {
            var recognizerResult = await Recognizer.RecognizeAsync(stepContext.Context, cancellationToken);
            var topIntent = recognizerResult?.GetTopScoringIntent();
            string strIntent = (topIntent != null) ? topIntent.Value.intent : "";
            if (strIntent != "")
            {
                switch (strIntent.ToLower())
                {
                    case "none":
                        await stepContext.EndDialogAsync(null, cancellationToken);
                        return await stepContext.BeginDialogAsync(nameof(LUISDefaultDialog), null, cancellationToken);
                    case "viewflights":
                        return await stepContext.BeginDialogAsync(nameof(ViewFlightsDialog), null, cancellationToken);
                    case "webcheckin":
                        return await stepContext.BeginDialogAsync(nameof(WebCheckInDialog), null, cancellationToken);
                    case "airporttour":
                        return await stepContext.BeginDialogAsync(nameof(AirportTourDialog), null, cancellationToken);
                    case "faq":
                        await stepContext.EndDialogAsync(null, cancellationToken);
                        return await stepContext.BeginDialogAsync(nameof(FAQDialog), null, cancellationToken);
                    case "trackmyflight":
                        return await stepContext.BeginDialogAsync(nameof(TrackMyflightDialog), null, cancellationToken);
                    case "viewall":
                        return await stepContext.BeginDialogAsync(nameof(ViewAllFlightsDialog), null, cancellationToken);
                    case "contactus":
                        return await stepContext.PromptAsync(nameof(TextPrompt), new PromptOptions { Prompt = MessageFactory.Text($"You may contact us, please visit this link for all contact details https://www.newdelhiairport.in/contactus. We'd be happy to assist you in any way \U0001F600 {Environment.NewLine} Please contact us with details listed here https://www.newdelhiairport.in/contactus. We'd be glad to be of service.") }, cancellationToken);
                    case "default_welcome_intent":
                        return await stepContext.BeginDialogAsync(nameof(DefaultMainDialog), null, cancellationToken);
                    case "positiveans":
                        await stepContext.EndDialogAsync(null, cancellationToken);
                        return await stepContext.BeginDialogAsync(nameof(DefaultMainDialog), null, cancellationToken);
                    case "negativeans":
                        await stepContext.PromptAsync(nameof(TextPrompt), new PromptOptions { Prompt = MessageFactory.Text($"Goodbye traveller! Enjoy your flight and have a good day \U0001F600") }, cancellationToken);
                        return await stepContext.EndDialogAsync(null, cancellationToken);
                    case "viewform":
                        await stepContext.EndDialogAsync(null, cancellationToken);
                        return await stepContext.BeginDialogAsync(nameof(WebCheckInFormDialog), null, cancellationToken);
                    case "fillvoice":
                       await stepContext.EndDialogAsync(null, cancellationToken);
                       return await stepContext.BeginDialogAsync(nameof(WebCheckInVoiceFormDialog), null, cancellationToken);
                    default:
                        return await stepContext.PromptAsync(nameof(TextPrompt), new PromptOptions { Prompt = MessageFactory.Text("Sorry, I don’t understand") }, cancellationToken);
                }
            }
            return await stepContext.PromptAsync(nameof(TextPrompt), new PromptOptions { Prompt = MessageFactory.Text("Sorry, I don’t understand") }, cancellationToken);
        }
    }
}
