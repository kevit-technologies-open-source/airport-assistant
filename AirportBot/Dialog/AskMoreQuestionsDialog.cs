﻿using AirPortBot.Dialog;
using Microsoft.Bot.Builder;
using Microsoft.Bot.Builder.AI.Luis;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Builder.Dialogs.Choices;
using Microsoft.Bot.Schema;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace AirportBot.Dialog
{
    public class AskMoreQuestionsDialog : ComponentDialog
    {
        private LuisRecognizer Recognizer { get; } = null;

        public AskMoreQuestionsDialog(LuisRecognizer luis)
      : base(nameof(AskMoreQuestionsDialog))
        {
            this.Recognizer = luis ?? throw new System.ArgumentNullException(nameof(luis));

            AddDialog(new ChoicePrompt(nameof(ChoicePrompt)));
            AddDialog(new TextPrompt(nameof(TextPrompt)));

            AddDialog(new WaterfallDialog(nameof(WaterfallDialog), new WaterfallStep[]
            {
                SelectOption,
                GotoOption,
            }));

            //AddDialog(new FAQDialog(luis));

            InitialDialogId = nameof(WaterfallDialog);
        }

        private static async Task<DialogTurnResult> SelectOption(WaterfallStepContext stepContext, CancellationToken cancellationToken)
        {
            return await stepContext.PromptAsync(nameof(ChoicePrompt),
               new PromptOptions
               {
                   Prompt = MessageFactory.Text("Would you like to ask me more questions?", InputHints.ExpectingInput),
                   RetryPrompt = MessageFactory.Text("That was not a valid choice, please select from given options."),
                   Choices = ChoiceFactory.ToChoices(new List<string> { "Yes", "No" }),
               }, cancellationToken);
        }

        private async Task<DialogTurnResult> GotoOption(WaterfallStepContext stepContext, CancellationToken cancellationToken)
        {
            var recognizerResult = await Recognizer.RecognizeAsync(stepContext.Context, cancellationToken);
            var topIntent = recognizerResult?.GetTopScoringIntent();
            string strIntent = (topIntent != null) ? topIntent.Value.intent : "";
            if (strIntent != "")
            {
                switch (strIntent.ToLower())
                {
                    case "positiveans":
                        await stepContext.EndDialogAsync(null, cancellationToken);
                        return await stepContext.BeginDialogAsync(nameof(FAQDialog), null, cancellationToken);
                    case "negativeans":
                        await stepContext.PromptAsync(nameof(TextPrompt), new PromptOptions { Prompt = MessageFactory.Text($"Goodbye traveller! Enjoy your flight and have a good day \U0001F600") }, cancellationToken);
                        return await stepContext.EndDialogAsync(null, cancellationToken);
                        //return await stepContext.BeginDialogAsync(nameof(DefaultMainDialog), null, cancellationToken);
                }
            }
            await stepContext.PromptAsync(nameof(TextPrompt), new PromptOptions { Prompt = MessageFactory.Text("Sorry, I don’t understand") }, cancellationToken);
            await stepContext.EndDialogAsync(null, cancellationToken);
            return await stepContext.BeginDialogAsync(nameof(DefaultMainDialog), null, cancellationToken);
        }
    }
}
