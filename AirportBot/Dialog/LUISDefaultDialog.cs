﻿using AirPortBot.Dialog;
using Microsoft.Bot.Builder;
using Microsoft.Bot.Builder.AI.Luis;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Builder.Dialogs.Choices;
using Microsoft.Bot.Schema;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace AirportBot.Dialog
{
    public class LUISDefaultDialog : ComponentDialog
    {
        private LuisRecognizer Recognizer { get; } = null;

        public LUISDefaultDialog(LuisRecognizer luis)
            : base(nameof(LUISDefaultDialog))
        {
            this.Recognizer = luis ?? throw new System.ArgumentNullException(nameof(luis));
            AddDialog(new WaterfallDialog(nameof(WaterfallDialog), new WaterfallStep[]
            {
               DefaultMessage,
               DefaultMessageResult
            }));

            AddDialog(new TextPrompt(nameof(TextPrompt)));
            AddDialog(new ChoicePrompt(nameof(ChoicePrompt)));
            AddDialog(new ViewFlightsDialog(luis));
            AddDialog(new WebCheckInDialog(luis));
            AddDialog(new AirportTourDialog(luis));
            AddDialog(new FAQDialog(luis));
            AddDialog(new TrackMyflightDialog(luis));
            AddDialog(new ViewAllFlightsDialog(luis));

            InitialDialogId = nameof(WaterfallDialog);
        }

        private static async Task<DialogTurnResult> DefaultMessage(WaterfallStepContext stepContext, CancellationToken cancellationToken)
        {
            return await stepContext.PromptAsync(nameof(ChoicePrompt),
            new PromptOptions
            {
                Prompt = MessageFactory.Text($"Sorry I don't understand your request {Environment.NewLine} What more may I assist you with"),
                RetryPrompt = MessageFactory.Text($"Sorry I don't understand your request {Environment.NewLine} What more may I assist you with"),
                Choices = new List<Choice>
                   {
                       new Choice
                       {
                            Value = "View Flights",
                            Synonyms = new List<string>
                            {
                                "View Flights",
                                "Flights",
                            },
                       },
                       new Choice
                       {
                            Value = "Web Check In",
                            Synonyms = new List<string>
                            {
                                "Web Check In",
                                "Check In",
                            },
                       },
                       new Choice
                       {
                            Value = "Airport Tour",
                            Synonyms = new List<string>
                            {
                                "Airport Tour",
                                "Tour",
                            },
                       },
                       new Choice
                       {
                            Value = "FAQ",
                            Synonyms = new List<string>
                            {
                                "FAQ",
                                "Asked Questions",
                                "faq",
                            },
                       },
                        new Choice
                       {
                            Value = "Track My Flights",
                            Synonyms = new List<string>
                            {
                                "Track My Flights",
                                "track my flights",
                                "track flights",
                                "Track Flights",
                                "Track",
                            },
                       }
                   }
            }, cancellationToken);
        }

        private async Task<DialogTurnResult> DefaultMessageResult(WaterfallStepContext stepContext, CancellationToken cancellationToken)
        {
            var recognizerResult = await Recognizer.RecognizeAsync(stepContext.Context, cancellationToken);
            var topIntent = recognizerResult?.GetTopScoringIntent();
            string strIntent = (topIntent != null) ? topIntent.Value.intent : "";
            if (strIntent != "")
            {
                switch (strIntent.ToLower())
                {
                    case "none":
                        await stepContext.EndDialogAsync(null, cancellationToken);
                        return await stepContext.BeginDialogAsync(nameof(LUISDefaultDialog), null, cancellationToken);
                    case "viewflights":
                        await stepContext.EndDialogAsync(null, cancellationToken);
                        return await stepContext.BeginDialogAsync(nameof(ViewFlightsDialog), null, cancellationToken);
                    case "webcheckin":
                        await stepContext.EndDialogAsync(null, cancellationToken);
                        return await stepContext.BeginDialogAsync(nameof(WebCheckInDialog), null, cancellationToken);
                    case "airporttour":
                        await stepContext.EndDialogAsync(null, cancellationToken);
                        return await stepContext.BeginDialogAsync(nameof(AirportTourDialog), null, cancellationToken);
                    case "faq":
                        await stepContext.EndDialogAsync(null, cancellationToken);
                        return await stepContext.BeginDialogAsync(nameof(FAQDialog), null, cancellationToken);
                    case "trackmyflight":
                        await stepContext.EndDialogAsync(null, cancellationToken);
                        return await stepContext.BeginDialogAsync(nameof(TrackMyflightDialog), null, cancellationToken);
                    case "viewall":
                        await stepContext.EndDialogAsync(null, cancellationToken);
                        return await stepContext.BeginDialogAsync(nameof(ViewAllFlightsDialog), null, cancellationToken);
                    case "contactus":
                        await stepContext.EndDialogAsync(null, cancellationToken);
                        await stepContext.PromptAsync(nameof(TextPrompt), new PromptOptions { Prompt = MessageFactory.Text($"You may contact us, please visit this link for all contact details https://www.newdelhiairport.in/contactus. We'd be happy to assist you in any way \U0001F600 {Environment.NewLine} Please contact us with details listed here https://www.newdelhiairport.in/contactus. We'd be glad to be of service.") }, cancellationToken);
                        return await stepContext.BeginDialogAsync(nameof(DefaultMainDialog), null, cancellationToken);
                    case "positiveans":
                        await stepContext.EndDialogAsync(null, cancellationToken);
                        return await stepContext.BeginDialogAsync(nameof(DefaultMainDialog), null, cancellationToken);
                    case "negativeans":
                        await stepContext.EndDialogAsync(null, cancellationToken);
                        await stepContext.PromptAsync(nameof(TextPrompt), new PromptOptions { Prompt = MessageFactory.Text($"Goodbye traveller! Enjoy your flight and have a good day \U0001F600") }, cancellationToken);
                        return await stepContext.BeginDialogAsync(nameof(DefaultMainDialog), null, cancellationToken);
                    case "welcomeairport":
                        await stepContext.EndDialogAsync(null, cancellationToken);
                        return await stepContext.BeginDialogAsync(nameof(DefaultMainDialog), null, cancellationToken);
                    default:
                        await stepContext.EndDialogAsync(null, cancellationToken);
                        return await stepContext.BeginDialogAsync(nameof(LUISDefaultDialog), null, cancellationToken);
                }
            }
            await stepContext.PromptAsync(nameof(TextPrompt), new PromptOptions { Prompt = MessageFactory.Text("Sorry, I don’t understand") }, cancellationToken);
            await stepContext.EndDialogAsync(null, cancellationToken);
            return await stepContext.BeginDialogAsync(nameof(DefaultMainDialog), null, cancellationToken);
        }
    }
}
