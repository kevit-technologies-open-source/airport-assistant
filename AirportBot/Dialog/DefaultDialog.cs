﻿using AirportBot.Dialog;
using Microsoft.Bot.Builder;
using Microsoft.Bot.Builder.AI.Luis;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Builder.Dialogs.Choices;
using Microsoft.Bot.Schema;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace AirPortBot.Dialog
{
    public class DefaultDialog  : ComponentDialog
    {
        private LuisRecognizer Recognizer { get; } = null;

        public DefaultDialog(LuisRecognizer luis)
        : base(nameof(DefaultDialog))
        {
            this.Recognizer = luis ?? throw new System.ArgumentNullException(nameof(luis));
            AddDialog(new WaterfallDialog(nameof(WaterfallDialog), new WaterfallStep[]
           {
               DefaultMessage,
               DefaultMessageResult
           }));

            AddDialog(new TextPrompt(nameof(TextPrompt)));
            AddDialog(new ChoicePrompt(nameof(ChoicePrompt)));

            InitialDialogId = nameof(WaterfallDialog);
        }

        private static async Task<DialogTurnResult> DefaultMessage(WaterfallStepContext stepContext, CancellationToken cancellationToken)
        {
            return await stepContext.PromptAsync(nameof(ChoicePrompt),
            new PromptOptions
            {
                Prompt = MessageFactory.Text("Alright, may I help you with anything else?", InputHints.ExpectingInput),
                Choices = ChoiceFactory.ToChoices(new List<string> { "Yes", "No" }),
            }, cancellationToken);
        }

        private async Task<DialogTurnResult> DefaultMessageResult(WaterfallStepContext stepContext, CancellationToken cancellationToken)
        {
            var recognizerResult = await Recognizer.RecognizeAsync(stepContext.Context, cancellationToken);
            var topIntent = recognizerResult?.GetTopScoringIntent();
            string strIntent = (topIntent != null) ? topIntent.Value.intent : "";
            if (strIntent != "")
            {
                switch (strIntent.ToLower())
                {
                     case "positiveans":
                            await stepContext.EndDialogAsync(null, cancellationToken);
                            return await stepContext.BeginDialogAsync(nameof(MainDialog), null, cancellationToken);
                    case "negativeans":
                        await stepContext.PromptAsync(nameof(TextPrompt), new PromptOptions { Prompt = MessageFactory.Text($"Goodbye traveller! Enjoy your flight and have a good day \U0001F600") }, cancellationToken);
                        return await stepContext.EndDialogAsync(null, cancellationToken);
                        //return await stepContext.BeginDialogAsync(nameof(DefaultMainDialog), null, cancellationToken);
                }
            }
            await stepContext.PromptAsync(nameof(TextPrompt), new PromptOptions { Prompt = MessageFactory.Text($"Sorry, I don’t understand {Environment.NewLine} You may say or choose one of the following options to explore") }, cancellationToken);
            return await stepContext.BeginDialogAsync(nameof(LUISDefaultDialog), null, cancellationToken);
        }
    }
}
