﻿using AirPortBot;
using AirPortBot.Dialog;
using Microsoft.Bot.Builder;
using Microsoft.Bot.Builder.AI.Luis;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Builder.Dialogs.Choices;
using Microsoft.Bot.Schema;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace AirportBot.Dialog
{
    public class DefaultMainDialog : ComponentDialog
    {
        private LuisRecognizer Recognizer { get; } = null;

        public DefaultMainDialog(LuisRecognizer luis)
      : base(nameof(DefaultMainDialog))
        {
            this.Recognizer = luis ?? throw new System.ArgumentNullException(nameof(luis));
            AddDialog(new WaterfallDialog(nameof(WaterfallDialog), new WaterfallStep[]
           {
               DefaultMessage,
               DefaultMessageResult
           }));

            AddDialog(new TextPrompt(nameof(TextPrompt)));
            AddDialog(new ChoicePrompt(nameof(ChoicePrompt)));
            AddDialog(new ViewFlightsDialog(luis));
            AddDialog(new WebCheckInDialog(luis));
            AddDialog(new AirportTourDialog(luis));
            AddDialog(new FAQDialog(luis));
            AddDialog(new TrackMyflightDialog(luis));
            AddDialog(new ViewAllFlightsDialog(luis));

            InitialDialogId = nameof(WaterfallDialog);
        }

        private static async Task<DialogTurnResult> DefaultMessage(WaterfallStepContext stepContext, CancellationToken cancellationToken)
        {
            return await stepContext.PromptAsync(nameof(ChoicePrompt),
            new PromptOptions
            {
                Prompt = MessageFactory.Text($"Please tell me, what may I assist you with? {Environment.NewLine} You may type your message or press on the mic to ask me for assistance."),
                RetryPrompt = MessageFactory.Text("That was not a valid choice, please select from given options."),
                Choices = new List<Choice>
                   {
                       new Choice
                       {
                            Value = "View Flights",
                            Synonyms = new List<string>
                            {
                                "View Flights",
                                "Flights",
                            },
                       },
                       new Choice
                       {
                            Value = "Web Check In",
                            Synonyms = new List<string>
                            {
                                "Web Check In",
                                "Check In",
                            },
                       },
                       new Choice
                       {
                            Value = "Airport Tour",
                            Synonyms = new List<string>
                            {
                                "Airport Tour",
                                "Tour",
                            },
                       },
                       new Choice
                       {
                            Value = "FAQ",
                            Synonyms = new List<string>
                            {
                                "FAQ",
                                "Asked Questions",
                                "faq",
                            },
                       }
                   }
            }, cancellationToken);
        }

        private async Task<DialogTurnResult> DefaultMessageResult(WaterfallStepContext stepContext, CancellationToken cancellationToken)
        {
            var recognizerResult = await Recognizer.RecognizeAsync(stepContext.Context, cancellationToken);
            var topIntent = recognizerResult?.GetTopScoringIntent();
            string strIntent = (topIntent != null) ? topIntent.Value.intent : "";
            if (strIntent != "")
            {
                switch (strIntent.ToLower())
                {
                    case "viewflights":
                        return await stepContext.BeginDialogAsync(nameof(ViewFlightsDialog), null, cancellationToken);
                    case "webcheckin":
                        return await stepContext.BeginDialogAsync(nameof(WebCheckInDialog), null, cancellationToken);
                    case "airporttour":
                        return await stepContext.BeginDialogAsync(nameof(AirportTourDialog), null, cancellationToken);
                    case "faq":
                        return await stepContext.BeginDialogAsync(nameof(FAQDialog), null, cancellationToken);
                    case "trackmyflight":
                        return await stepContext.BeginDialogAsync(nameof(TrackMyflightDialog), null, cancellationToken);
                    case "viewall":
                        return await stepContext.BeginDialogAsync(nameof(ViewAllFlightsDialog), null, cancellationToken);
                    case "contactus":
                        return await stepContext.PromptAsync(nameof(TextPrompt), new PromptOptions { Prompt = MessageFactory.Text($"You may contact us, please visit this link for all contact details https://www.newdelhiairport.in/contactus. We'd be happy to assist you in any way :D {Environment.NewLine} Please contact us with details listed here https://www.newdelhiairport.in/contactus. We'd be glad to be of service.") }, cancellationToken);
                    case "positiveans":
                        await stepContext.EndDialogAsync(null, cancellationToken);
                        return await stepContext.ReplaceDialogAsync(nameof(DefaultMainDialog), null, cancellationToken);
                    case "negativeans":
                        await stepContext.PromptAsync(nameof(TextPrompt), new PromptOptions { Prompt = MessageFactory.Text($"Goodbye traveller! Enjoy your flight and have a good day \U0001F600") }, cancellationToken);
                        return await stepContext.EndDialogAsync(null, cancellationToken);
                    case "welcomeairport":
                        await stepContext.EndDialogAsync(null, cancellationToken);
                        return await stepContext.ReplaceDialogAsync(nameof(DefaultMainDialog), null, cancellationToken);
                    default:
                        return await stepContext.PromptAsync(nameof(TextPrompt), new PromptOptions { Prompt = MessageFactory.Text("Sorry, I don’t understand") }, cancellationToken);
                }
            }
            await stepContext.PromptAsync(nameof(TextPrompt), new PromptOptions { Prompt = MessageFactory.Text("Sorry, I don’t understand") }, cancellationToken);
            await stepContext.EndDialogAsync(null, cancellationToken);
            return await stepContext.BeginDialogAsync(nameof(DefaultMainDialog), null, cancellationToken);
        }
    }
}
