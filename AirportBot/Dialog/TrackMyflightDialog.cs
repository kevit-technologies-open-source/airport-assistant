﻿using AirPortBot;
using AirPortBot.Dialog;
using Microsoft.Bot.Builder;
using Microsoft.Bot.Builder.AI.Luis;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Builder.Dialogs.Choices;
using Microsoft.Bot.Schema;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace AirportBot.Dialog
{
    public class TrackMyflightDialog : ComponentDialog
    {
        private LuisRecognizer Recognizer { get; } = null;

        public TrackMyflightDialog(LuisRecognizer luis)
         : base(nameof(TrackMyflightDialog))
        {
            this.Recognizer = luis ?? throw new System.ArgumentNullException(nameof(luis));
            var waterfallSteps = new WaterfallStep[]
           {
                AskForFlightnumber,
                SelectedFlightNumberStepAsync,
                OtherServicesStepAsync,
           };
            AddDialog(new WaterfallDialog(nameof(WaterfallDialog), waterfallSteps));
            AddDialog(new TextPrompt(nameof(TextPrompt)));
            AddDialog(new ChoicePrompt(nameof(ChoicePrompt)));
            AddDialog(new AirportTourDialog(luis));
            AddDialog(new WebCheckInDialog(luis));
            //AddDialog(new LUISDefaultDialog(luis));

            InitialDialogId = nameof(WaterfallDialog);
        }

        private async Task<DialogTurnResult> AskForFlightnumber(WaterfallStepContext stepContext, CancellationToken cancellationToken)
        {
            return await stepContext.PromptAsync(nameof(TextPrompt), new PromptOptions { Prompt = MessageFactory.Text($"Sure, please tell me the number of your flight?") }, cancellationToken);
        }

        private async Task<DialogTurnResult> SelectedFlightNumberStepAsync(WaterfallStepContext stepContext, CancellationToken cancellationToken)
        {
            stepContext.Values["FlightNumber"] = stepContext.Result;
            var flightNumber = stepContext.Values["FlightNumber"];
            if (flightNumber != null)
            {
                await stepContext.Context.SendActivityAsync(MessageFactory.Text($"Here are the details of your flight {Environment.NewLine}"), cancellationToken: cancellationToken);
                var attachments = new List<Attachment>();
                var reply = MessageFactory.Attachment(attachments);
                reply.Attachments.Add(Common.CreateAdaptiveCardAttachment(Path.Combine(".", "Resources", "TrackMyFlights.json")));
                await stepContext.Context.SendActivityAsync(reply, cancellationToken);
                return await stepContext.PromptAsync(nameof(ChoicePrompt),
                new PromptOptions
                {
                    Prompt = MessageFactory.Text("May I help you with anything else today? Here are some things to explore with me"),
                    RetryPrompt = MessageFactory.Text("That was not a valid choice, please select from given options."),
                    Choices = new List<Choice>
                   {
                       new Choice
                       {
                            Value = "Airport Tour",
                            Synonyms = new List<string>
                            {
                                "Tour",
                                "Airport Tour",
                            },
                       },
                       new Choice
                       {
                            Value = "Web Check In",
                            Synonyms = new List<string>
                            {
                                "Web Check In",
                                "Check In",
                            },
                       },
                       new Choice
                       {
                            Value = "FAQ",
                            Synonyms = new List<string>
                            {
                                "FAQ",
                                "faq",
                                "Faq",
                            },
                       },
                       new Choice
                       {
                            Value = "No",
                            Synonyms = new List<string>
                            {
                                "No",
                                "No Thanks",
                                "No Thank You",
                                "no",
                                "no thanks",
                            },
                       },
                   }
                }, cancellationToken);
            }
            else
            {
                return await stepContext.PromptAsync(nameof(TextPrompt), new PromptOptions { Prompt = MessageFactory.Text($"Please tell me the number of your flight?", InputHints.ExpectingInput) }, cancellationToken);
            }
        }

        private async Task<DialogTurnResult> OtherServicesStepAsync(WaterfallStepContext stepContext, CancellationToken cancellationToken)
        {
            var recognizerResult = await Recognizer.RecognizeAsync(stepContext.Context, cancellationToken);
            var topIntent = recognizerResult?.GetTopScoringIntent();
            string strIntent = (topIntent != null) ? topIntent.Value.intent : "";
            if (strIntent != "")
            {
                switch (strIntent.ToLower())
                {
                    case "none":
                        await stepContext.EndDialogAsync(null, cancellationToken);
                        return await stepContext.BeginDialogAsync(nameof(LUISDefaultDialog), null, cancellationToken);
                    case "viewflights":
                        await stepContext.EndDialogAsync(null, cancellationToken);
                        return await stepContext.BeginDialogAsync(nameof(ViewFlightsDialog), null, cancellationToken);
                    case "webcheckin":
                        await stepContext.EndDialogAsync(null, cancellationToken);
                        return await stepContext.BeginDialogAsync(nameof(WebCheckInDialog), null, cancellationToken);
                    case "airporttour":
                        await stepContext.EndDialogAsync(null, cancellationToken);
                        return await stepContext.BeginDialogAsync(nameof(AirportTourDialog), null, cancellationToken);
                    case "faq":
                        await stepContext.EndDialogAsync(null, cancellationToken);
                        return await stepContext.BeginDialogAsync(nameof(FAQDialog), null, cancellationToken);
                    case "trackmyflight":
                        await stepContext.EndDialogAsync(null, cancellationToken);
                        return await stepContext.BeginDialogAsync(nameof(TrackMyflightDialog), null, cancellationToken);
                    case "viewall":
                        await stepContext.EndDialogAsync(null, cancellationToken);
                        return await stepContext.BeginDialogAsync(nameof(ViewAllFlightsDialog), null, cancellationToken);
                    case "contactus":
                        await stepContext.EndDialogAsync(null, cancellationToken);
                        return await stepContext.PromptAsync(nameof(TextPrompt), new PromptOptions { Prompt = MessageFactory.Text($"You may contact us, please visit this link for all contact details https://www.newdelhiairport.in/contactus. We'd be happy to assist you in any way \U0001F600 {Environment.NewLine} Please contact us with details listed here https://www.newdelhiairport.in/contactus. We'd be glad to be of service.") }, cancellationToken);
                    case "negativeans":
                        await stepContext.PromptAsync(nameof(TextPrompt), new PromptOptions { Prompt = MessageFactory.Text($"Goodbye traveller! Enjoy your flight and have a good day \U0001F600") }, cancellationToken);
                        return await stepContext.EndDialogAsync(null, cancellationToken);
                    default:
                        return await stepContext.PromptAsync(nameof(TextPrompt), new PromptOptions { Prompt = MessageFactory.Text("Sorry, I don’t understand") }, cancellationToken);
                }
            }
            return await stepContext.PromptAsync(nameof(TextPrompt), new PromptOptions { Prompt = MessageFactory.Text("Sorry, I don’t understand") }, cancellationToken);
        }
    }
}
