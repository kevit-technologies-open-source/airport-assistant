﻿using AirportBot.Dialog;
using Microsoft.Bot.Builder;
using Microsoft.Bot.Builder.AI.Luis;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Schema;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace AirPortBot.Dialog
{
    public class FAQDialog : ComponentDialog
    {
        private LuisRecognizer Recognizer { get; } = null;

        public FAQDialog(LuisRecognizer luis)
             : base(nameof(FAQDialog))
        {
            this.Recognizer = luis ?? throw new System.ArgumentNullException(nameof(luis));

            var waterfallSteps = new WaterfallStep[]
            {
                FAQInit,
                FAQUserQuestion,
            };

            AddDialog(new WaterfallDialog(nameof(WaterfallDialog), waterfallSteps));
            AddDialog(new TextPrompt(nameof(TextPrompt)));
            AddDialog(new ConfirmPrompt(nameof(ConfirmPrompt)));
            AddDialog(new AskMoreQuestionsDialog(luis));
            AddDialog(new DefaultDialog(luis));

            InitialDialogId = nameof(WaterfallDialog);
        }

        private async Task<DialogTurnResult> FAQInit(WaterfallStepContext stepContext, CancellationToken cancellationToken)
        {
            return await stepContext.PromptAsync(nameof(TextPrompt), new PromptOptions { Prompt = MessageFactory.Text($"Please go ahead and ask me a question! I'd be happy to answer your queries \U0001F600 {Environment.NewLine} You may ask me anything regarding the airport facilities, procedures or details about contacting us. {Environment.NewLine} So, what may I assist you with?") }, cancellationToken);
        }

        private async Task<DialogTurnResult> FAQUserQuestion(WaterfallStepContext stepContext, CancellationToken cancellationToken)
        {
            var recognizerResult = await Recognizer.RecognizeAsync(stepContext.Context, cancellationToken);
            var topIntent = recognizerResult?.GetTopScoringIntent();
            string strIntent = (topIntent != null) ? topIntent.Value.intent : "";
            double dblIntentScore = (topIntent != null) ? topIntent.Value.score : 0.0;
            if (strIntent != "" && dblIntentScore >= 0.09)
            {
                string Msg = string.Empty;
                switch (strIntent.ToLower())
                {
                    case "none":
                        await stepContext.PromptAsync(nameof(TextPrompt), new PromptOptions { Prompt = MessageFactory.Text($"Sorry, I don’t understand {Environment.NewLine} You may ask me anything regarding the airport facilities, procedures or details about contacting us.") }, cancellationToken);
                        await stepContext.EndDialogAsync(null, cancellationToken);
                        return await stepContext.BeginDialogAsync(nameof(AskMoreQuestionsDialog), null, cancellationToken);
                    case "knowledge_airportfaq":
                        await stepContext.PromptAsync(nameof(TextPrompt), new PromptOptions { Prompt = MessageFactory.Text($"Occasionally, the RVR at runway drops below 125m, which normally is minima for most of the aircrafts to depart but is above 50m, (minimum to land). Thus aircrafts wait for take off until there is improvement to the required level of RVR. However arriving aircrafts can land up to visibilities of 50m with the help of CAT IIIB ILS systems at IGI Airport.") }, cancellationToken);
                        await stepContext.EndDialogAsync(null, cancellationToken);
                        return await stepContext.BeginDialogAsync(nameof(AskMoreQuestionsDialog), null, cancellationToken);
                    case "knowledge_airportfaq2":
                        await stepContext.PromptAsync(nameof(TextPrompt), new PromptOptions { Prompt = MessageFactory.Text($"Yes, but according to the guidelines of the countries") }, cancellationToken);
                        await stepContext.EndDialogAsync(null, cancellationToken);
                        return await stepContext.BeginDialogAsync(nameof(AskMoreQuestionsDialog), null, cancellationToken);
                    case "knowledge_airportfaq3":
                        await stepContext.PromptAsync(nameof(TextPrompt), new PromptOptions { Prompt = MessageFactory.Text($"You are required to carry a doctor's prescription for medicines that are to be carried on board. Passengers are advised to get in touch with their respective airlines regarding the same.") }, cancellationToken);
                        await stepContext.EndDialogAsync(null, cancellationToken);
                        return await stepContext.BeginDialogAsync(nameof(AskMoreQuestionsDialog), null, cancellationToken);
                    case "knowledge_airportfaq4":
                        await stepContext.PromptAsync(nameof(TextPrompt), new PromptOptions { Prompt = MessageFactory.Text($"All shops at the airport accept MasterCard, Visa and American Express.") }, cancellationToken);
                        await stepContext.EndDialogAsync(null, cancellationToken);
                        return await stepContext.BeginDialogAsync(nameof(AskMoreQuestionsDialog), null, cancellationToken);
                    case "knowledge_airportfaq5":
                        await stepContext.PromptAsync(nameof(TextPrompt), new PromptOptions { Prompt = MessageFactory.Text($"In most cases it is safe for women, with a normal pregnancy, to travel by air. However, it is best to check with your doctor before you plan your travel.") }, cancellationToken);
                        await stepContext.EndDialogAsync(null, cancellationToken);
                        return await stepContext.BeginDialogAsync(nameof(AskMoreQuestionsDialog), null, cancellationToken);
                    case "knowledge_airportfaq6":
                        await stepContext.PromptAsync(nameof(TextPrompt), new PromptOptions { Prompt = MessageFactory.Text($"Yes and can use below currency also to buy the goods from Duty Free shops USD,Euros,INR,Sterling pound") }, cancellationToken);
                        await stepContext.EndDialogAsync(null, cancellationToken);
                        return await stepContext.BeginDialogAsync(nameof(AskMoreQuestionsDialog), null, cancellationToken);
                    case "knowledge_airportfaq7":
                        await stepContext.PromptAsync(nameof(TextPrompt), new PromptOptions { Prompt = MessageFactory.Text($"In an effort to keep the environment clean, all terminals at Indira Gandhi International Airport provide dedicated smoking lounges. These lounges are located inside the terminals, after security screening. Smoking in any other area is prohibited.") }, cancellationToken);
                        await stepContext.EndDialogAsync(null, cancellationToken);
                        return await stepContext.BeginDialogAsync(nameof(AskMoreQuestionsDialog), null, cancellationToken);
                    case "knowledge_airportfaq8":
                        await stepContext.PromptAsync(nameof(TextPrompt), new PromptOptions { Prompt = MessageFactory.Text($"In the event of a medical emergency, well trained doctors and paramedics are available from the emergency treatment centre run by Medanta Hospital, the Medicity. These doctors and paramedics are available round the clock. The Medanta Medical Centres are equipped with emergency treatment centre for passengers & visitors alike. Ambulances are provided to cater to any severe medical emergency. Medanta Medical facility is available at T-1, T-2 and T-3. Fortis Medical Facility is also available at T-3.") }, cancellationToken);
                        await stepContext.EndDialogAsync(null, cancellationToken);
                        return await stepContext.BeginDialogAsync(nameof(AskMoreQuestionsDialog), null, cancellationToken);
                    case "knowledge_airportfaq9":
                        await stepContext.PromptAsync(nameof(TextPrompt), new PromptOptions { Prompt = MessageFactory.Text($"Foreign Exchange counters and ATMs are available at all terminals of Indira Gandhi International Airport.") }, cancellationToken);
                        await stepContext.EndDialogAsync(null, cancellationToken);
                        return await stepContext.BeginDialogAsync(nameof(AskMoreQuestionsDialog), null, cancellationToken);
                    case "knowledge_airportfaq10":
                        await stepContext.PromptAsync(nameof(TextPrompt), new PromptOptions { Prompt = MessageFactory.Text($"Flight Information Display System (FIDS) is displayed on the homepage of our website, which provides the latest updates regarding the arrival and departure of all scheduled flights. Flight information can also be obtained by contacting the airline directly. However, if you wish, you may contact our call center on +91 124 3376000. Call center service is available 24 hours 7 days in a week.{Environment.NewLine} You may also View Flights in our virtual agent, just say View flights.") }, cancellationToken);
                        await stepContext.EndDialogAsync(null, cancellationToken);
                        return await stepContext.BeginDialogAsync(nameof(AskMoreQuestionsDialog), null, cancellationToken);
                    case "knowledge_airportfaq11":
                        await stepContext.PromptAsync(nameof(TextPrompt), new PromptOptions { Prompt = MessageFactory.Text($"Inter terminal transfer coaches are provided which connect passengers between Terminal 3 and Terminal 1D . A Inter Terminal Transfer Helpdesk is located outside the arrival terminals for assistance.") }, cancellationToken);
                        await stepContext.EndDialogAsync(null, cancellationToken);
                        return await stepContext.BeginDialogAsync(nameof(AskMoreQuestionsDialog), null, cancellationToken);
                    case "knowledge_airportfaq12":
                        await stepContext.PromptAsync(nameof(TextPrompt), new PromptOptions { Prompt = MessageFactory.Text($"Indira Gandhi International Airport is very well connected with the City and National Capital Region (NCR). Passenger can reach airport through Taxi, City Bus and Metro Rail.Please check the link below: http://www.newdelhiairport.in/to-and-from-airport") }, cancellationToken);
                        await stepContext.EndDialogAsync(null, cancellationToken);
                        return await stepContext.BeginDialogAsync(nameof(AskMoreQuestionsDialog), null, cancellationToken);
                    case "knowledge_airportfaq13":
                        await stepContext.PromptAsync(nameof(TextPrompt), new PromptOptions { Prompt = MessageFactory.Text($"Please click the link {Environment.NewLine} https://jobsatgmr.gmrgroup.in/GMRCareers/client/vacancies.aspx to find out the jobs available with Delhi International Airport Ltd.{Environment.NewLine} BEWARE OF FAKE JOB OFFERS GMR Group does not appoint agents to make job offers on behalf of the company.Please report such instances to info@gmrgroup.in.") }, cancellationToken);
                        await stepContext.EndDialogAsync(null, cancellationToken);
                        return await stepContext.BeginDialogAsync(nameof(AskMoreQuestionsDialog), null, cancellationToken);
                    case "knowledge_airportfaq14":
                        await stepContext.PromptAsync(nameof(TextPrompt), new PromptOptions { Prompt = MessageFactory.Text($"Its depends on the specific requirements of the countries you are travelling. Please look into the country' liquor requirements or policies before travelling.") }, cancellationToken);
                        await stepContext.EndDialogAsync(null, cancellationToken);
                        return await stepContext.BeginDialogAsync(nameof(AskMoreQuestionsDialog), null, cancellationToken);
                    case "knowledge_airportfaq16":
                        await stepContext.PromptAsync(nameof(TextPrompt), new PromptOptions { Prompt = MessageFactory.Text($"Please arrive 90 minutes prior to departure of your domestic flights and 3 hours prior to departure for your international flights.") }, cancellationToken);
                        await stepContext.EndDialogAsync(null, cancellationToken);
                        return await stepContext.BeginDialogAsync(nameof(AskMoreQuestionsDialog), null, cancellationToken);
                    case "knowledge_airportfaq17":
                        await stepContext.PromptAsync(nameof(TextPrompt), new PromptOptions { Prompt = MessageFactory.Text($"In the first instance, you must always contact your airline for information on flight arrivals and departures. When there is fog impact, airlines usually combine flights on a larger aircraft or reallocate aircraft operating a specific flight. Customers should check flight status before starting from home in case of fog. Passenger are also advised to maintain calm and cooperate with Airport staff who does their best in handling the challenge imposed on them by mother nature.") }, cancellationToken);
                        await stepContext.EndDialogAsync(null, cancellationToken);
                        return await stepContext.BeginDialogAsync(nameof(AskMoreQuestionsDialog), null, cancellationToken);
                    case "knowledge_airportfaq18":
                        await stepContext.PromptAsync(nameof(TextPrompt), new PromptOptions { Prompt = MessageFactory.Text($"Yes, vehicles are permitted only for active loading and unloading of passengers. Vehicles left unattended at the curb will be towed and fined.") }, cancellationToken);
                        await stepContext.EndDialogAsync(null, cancellationToken);
                        return await stepContext.BeginDialogAsync(nameof(AskMoreQuestionsDialog), null, cancellationToken);
                    case "knowledge_airportfaq19":
                        await stepContext.PromptAsync(nameof(TextPrompt), new PromptOptions { Prompt = MessageFactory.Text($"The Left Luggage Facility is available in T3 and is located in the Multi Car Parking Facility, across from the terminal.") }, cancellationToken);
                        await stepContext.EndDialogAsync(null, cancellationToken);
                        return await stepContext.BeginDialogAsync(nameof(AskMoreQuestionsDialog), null, cancellationToken);
                    case "knowledge_airportfaq20":
                        await stepContext.PromptAsync(nameof(TextPrompt), new PromptOptions { Prompt = MessageFactory.Text($"The Holiday Inn Express New Delhi Airport Transit Hotel is conveniently located within the airport's sleek new Terminal 3. Click here for booking {Environment.NewLine} http://www.newdelhiairport.in/airport-hotel {Environment.NewLine} .Other airport has a self - contained aero city located two kms away from the terminals.A selection of accommodation is provided.Please book directly with the hotels.{Environment.NewLine} There are also sleeping pods available at the airport and can be booked here {Environment.NewLine} https://www.newdelhiairport.in/sleeping-pod") }, cancellationToken);
                        await stepContext.EndDialogAsync(null, cancellationToken);
                        return await stepContext.BeginDialogAsync(nameof(AskMoreQuestionsDialog), null, cancellationToken);
                    case "knowledge_airportfaq21":
                        await stepContext.PromptAsync(nameof(TextPrompt), new PromptOptions { Prompt = MessageFactory.Text($"You could try the various Food and Beverage and retail options available at the terminals. Just say Airport Tour to explore your options. {Environment.NewLine} There are rest and shower facilities also available at the international terminal. {Environment.NewLine} Airlines delegate personnel to address your queries and concerns. Also our professional Terminal team would be happy to reach out to help you.") }, cancellationToken);
                        await stepContext.EndDialogAsync(null, cancellationToken);
                        return await stepContext.BeginDialogAsync(nameof(AskMoreQuestionsDialog), null, cancellationToken);
                    case "knowledge_airportfaq22":
                        await stepContext.PromptAsync(nameof(TextPrompt), new PromptOptions { Prompt = MessageFactory.Text($"Please log on to http://www.newdelhiairport.in/contactus and share your feedback/suggestions") }, cancellationToken);
                        await stepContext.EndDialogAsync(null, cancellationToken);
                        return await stepContext.BeginDialogAsync(nameof(AskMoreQuestionsDialog), null, cancellationToken);
                    case "knowledge_airportfaq23":
                        await stepContext.PromptAsync(nameof(TextPrompt), new PromptOptions { Prompt = MessageFactory.Text($"All the unclaimed property found in the terminals & its vicinity are deposited in the Office of Airport Manager of respective terminal. Within 24 hours, lost property is deposited in Materials Management Department of the airport. The claimant should carry a photo-copy of their boarding card/ticket & an original government issued identification card to collect the property from the airport. {Environment.NewLine}If you have lost or misplaced any of your valuable at the airport only, our Lost and Found department is available to assist you please visit us at https://www.newdelhiairport.in/airport-guides/lost-and-found {Environment.NewLine} You may address your concern on lost items to lostproperty.dial@gmrgroup.in or Phone : +91-011-42489616 or +91-011-42489617 ") }, cancellationToken);
                        await stepContext.EndDialogAsync(null, cancellationToken);
                        return await stepContext.BeginDialogAsync(nameof(AskMoreQuestionsDialog), null, cancellationToken);
                    case "knowledge_airportfaq24":
                        return await stepContext.PromptAsync(nameof(TextPrompt), new PromptOptions { Prompt = MessageFactory.Text($"All Airlines provide options to their passengers on what they can do if their flight is cancelled as per Individual airline policies varying from rebooking, refunding or providing alternate modes of transport. Passengers can avail these solutions as considered best. ") }, cancellationToken);
                    case "knowledge_airportfaq25":
                        await stepContext.PromptAsync(nameof(TextPrompt), new PromptOptions { Prompt = MessageFactory.Text($"Self driven cars can be parked at the Multi Level Car Park located across from Terminal 3 and the Surface Car Park located across from Terminal 1. {Environment.NewLine} Please click  http://www.newdelhiairport.in/parking and check all details pertaining to parking at the airport. ") }, cancellationToken);
                        await stepContext.EndDialogAsync(null, cancellationToken);
                        return await stepContext.BeginDialogAsync(nameof(AskMoreQuestionsDialog), null, cancellationToken);
                    case "knowledge_airportfaq26":
                        await stepContext.PromptAsync(nameof(TextPrompt), new PromptOptions { Prompt = MessageFactory.Text($"Delhi IGI Airport has state-of-art CAT IIIB Instrument landing systems equipped on Runways 11, 29 and 28. These systems are the most advanced of its class and provide the guidance required for pilots to land an aircraft in low visibility conditions. While the equipment are available at airport, it also requires aircraft to be equipped on board and pilot to be trained to utilize these systems, ie; be CAT IIIB compliant. Most modern aircraft are CAT IIIB compliant and Airlines are required to roster CATIIIB qualified pilots during fog.") }, cancellationToken);
                        await stepContext.EndDialogAsync(null, cancellationToken);
                        return await stepContext.BeginDialogAsync(nameof(AskMoreQuestionsDialog), null, cancellationToken);
                    case "knowledge_airportfaq27":
                        await stepContext.PromptAsync(nameof(TextPrompt), new PromptOptions { Prompt = MessageFactory.Text($"For the privacy of mothers who wish to nurse their infants, a well-equipped Child Care Lounge with toys, baby cot is available at all terminals. In case of more information please visit here https://www.newdelhiairport.in/airport-guides/travelling-with-kids. We make sure your experience with kids at the airport is comfortable.") }, cancellationToken);
                        await stepContext.EndDialogAsync(null, cancellationToken);
                        return await stepContext.BeginDialogAsync(nameof(AskMoreQuestionsDialog), null, cancellationToken);
                    case "knowledge_airportfaq28":
                        await stepContext.PromptAsync(nameof(TextPrompt), new PromptOptions { Prompt = MessageFactory.Text($"Passengers holding a valid ticket to travel within the next 24 hours can access the visitors lounge at T3 through Gate 1 and Gate 8. Please note that visitor entry to terminals and lounges may be restricted without prior notice due to security or operational reasons.{Environment.NewLine} For a better experience, you may also book a lounge with us at https://www.newdelhiairport.in/book-lounge") }, cancellationToken);
                        await stepContext.EndDialogAsync(null, cancellationToken);
                        return await stepContext.BeginDialogAsync(nameof(AskMoreQuestionsDialog), null, cancellationToken);
                    case "knowledge_airportfaq29":
                        await stepContext.PromptAsync(nameof(TextPrompt), new PromptOptions { Prompt = MessageFactory.Text($"The airport is Wi-Fi enabled. A valid Indian mobile number is required to access the internet.") }, cancellationToken);
                        await stepContext.EndDialogAsync(null, cancellationToken);
                        return await stepContext.BeginDialogAsync(nameof(AskMoreQuestionsDialog), null, cancellationToken);
                    case "knowledge_airportfaq30":
                        await stepContext.PromptAsync(nameof(TextPrompt), new PromptOptions { Prompt = MessageFactory.Text($"Delhi Airport is well equipped to provide services to passengers/patrons with reduced mobility and special needs, click to know more https://www.newdelhiairport.in/airport-guides/special-assistance-prm") }, cancellationToken);
                        await stepContext.EndDialogAsync(null, cancellationToken);
                        return await stepContext.BeginDialogAsync(nameof(AskMoreQuestionsDialog), null, cancellationToken);
                    case "knowledge_airportfaq31":
                        await stepContext.PromptAsync(nameof(TextPrompt), new PromptOptions { Prompt = MessageFactory.Text($"You may register a complaint regarding the defective/damaged good purchase against the shop from where you made your purchase.You may also lodge a complaint by sending a detailed mail to  feedback.igiairport@gmrgroup.in. {Environment.NewLine} You may fill the following form and register your complaint with us https://www.newdelhiairport.in/contactus. Please feel free to contact us.") }, cancellationToken);
                        await stepContext.EndDialogAsync(null, cancellationToken);
                        return await stepContext.BeginDialogAsync(nameof(AskMoreQuestionsDialog), null, cancellationToken);
                    case "knowledge_airportfaq32":
                        await stepContext.PromptAsync(nameof(TextPrompt), new PromptOptions { Prompt = MessageFactory.Text($"Kindly contact the airline you have flown and register a complaint with them. It is the airlines responsibility to assist in such a matter.") }, cancellationToken);
                        await stepContext.EndDialogAsync(null, cancellationToken);
                        return await stepContext.BeginDialogAsync(nameof(AskMoreQuestionsDialog), null, cancellationToken);
                    case "knowledge_airportfaq33":
                        await stepContext.PromptAsync(nameof(TextPrompt), new PromptOptions { Prompt = MessageFactory.Text($"Airline schedules are dependent on aircrafts arriving and departing on time. Any disruption to this close knit operation will result in cascading effect to the entire network, flyers missing connecting flights, havoc to ground resource planning (stands and airfield) and stress to personnel trying to get flyers moving. This leads to Airports being overcrowded, customer service challenges and overall stress on the infrastructure.") }, cancellationToken);
                        await stepContext.EndDialogAsync(null, cancellationToken);
                        return await stepContext.BeginDialogAsync(nameof(AskMoreQuestionsDialog), null, cancellationToken);
                    case "knowledge_airportfaq34":
                        await stepContext.PromptAsync(nameof(TextPrompt), new PromptOptions { Prompt = MessageFactory.Text($"An unaccompanied minor is a child without the presence of a legal guardian. This term is used in immigration law and in airline policies. The specific definition varies from country to country and from airline to airline. Please check with the airline for further information.") }, cancellationToken);
                        await stepContext.EndDialogAsync(null, cancellationToken);
                        return await stepContext.BeginDialogAsync(nameof(AskMoreQuestionsDialog), null, cancellationToken);
                    case "knowledge_airportfaq35":
                        await stepContext.PromptAsync(nameof(TextPrompt), new PromptOptions { Prompt = MessageFactory.Text($"Fog trends are dynamic. Flight operations are to a large extent determined by this trend. Aircrafts often wait their time out either hovering above or on ground to land or take off as and when an opportunity arises. Since the intensity and lifting time are difficult to predict, all stakeholders are asked to be ready to utilize favorable conditions when they occur.") }, cancellationToken);
                        await stepContext.EndDialogAsync(null, cancellationToken);
                        return await stepContext.BeginDialogAsync(nameof(AskMoreQuestionsDialog), null, cancellationToken);
                    case "knowledge_airportfaq36":
                        await stepContext.PromptAsync(nameof(TextPrompt), new PromptOptions { Prompt = MessageFactory.Text($"Each airline has a definitive guideline with regards to baggage. Kindly contact your airline for all questions related to carry on and or check in baggage.") }, cancellationToken);
                        await stepContext.EndDialogAsync(null, cancellationToken);
                        return await stepContext.BeginDialogAsync(nameof(AskMoreQuestionsDialog), null, cancellationToken);
                    case "knowledge_airportfaq38":
                        await stepContext.PromptAsync(nameof(TextPrompt), new PromptOptions { Prompt = MessageFactory.Text($"Although aircraft and airport technology is continually evolving, operations during fog is limited to an extent. There is a minimum visibility required for pilots to safely land an aircraft in dense fog. Aircraft can land in up to 50 m RVR (Runway Visual Range) and normally take off if RVR is above 125 m. Take off can also take place if RVR is above 75 m if aircraft is equipped with approved lateral guidance system.") }, cancellationToken);
                        await stepContext.EndDialogAsync(null, cancellationToken);
                        return await stepContext.BeginDialogAsync(nameof(AskMoreQuestionsDialog), null, cancellationToken);
                    case "knowledge_airportfaq39":
                        await stepContext.PromptAsync(nameof(TextPrompt), new PromptOptions { Prompt = MessageFactory.Text($"One of the most frustrating aspects of air travel is a delay caused by inclement weather, in particular fog. The frustration is shared by airports, airlines and its ground handlers. Airlines may face several issues with fog causing delayed incoming aircraft, departure restrictions, aircraft taxiing issues, logistics hindrances etc. Also weather at destination is crucial. Even if an aircraft is able to depart in bad weather, it must be equally safe for it to land at its destination airport affected by weather. It is in the interest of rationale or passenger safety that flights are cancelled or delayed. Airlines provide options to passengers as per their individual policies based on Govt. guidelines.") }, cancellationToken);
                        await stepContext.EndDialogAsync(null, cancellationToken);
                        return await stepContext.BeginDialogAsync(nameof(AskMoreQuestionsDialog), null, cancellationToken);
                    case "knowledge_airportfaq40":
                        await stepContext.PromptAsync(nameof(TextPrompt), new PromptOptions { Prompt = MessageFactory.Text($"Fog can be considered as a low-lying cloud of water droplets or ice crystals suspended in the air above ground level which causes obstruction to visibility. It is a common phenomenon during the winter season in Delhi mid-December to February and impact airport operations to a great extent.") }, cancellationToken);
                        await stepContext.EndDialogAsync(null, cancellationToken);
                        return await stepContext.BeginDialogAsync(nameof(AskMoreQuestionsDialog), null, cancellationToken);
                    case "knowledge_airportfaqterminals":
                        await stepContext.PromptAsync(nameof(TextPrompt), new PromptOptions { Prompt = MessageFactory.Text($"Indira Gandhi International Airport has the following terminals: {Environment.NewLine} Terminal 1D {Environment.NewLine} Terminal 1C {Environment.NewLine} Terminal 2 {Environment.NewLine} Terminal 3 {Environment.NewLine} Cargo Terminal {Environment.NewLine} You may also view terminals guide using Airport Tour feature on this agent. Just say airport tour.") }, cancellationToken);
                        await stepContext.EndDialogAsync(null, cancellationToken);
                        return await stepContext.BeginDialogAsync(nameof(AskMoreQuestionsDialog), null, cancellationToken);
                    //case "trackmyflight":
                    //    await stepContext.EndDialogAsync(null, cancellationToken);
                    //    return await stepContext.BeginDialogAsync(nameof(ViewFlightsDialog), null, cancellationToken);
                    //case "viewall":
                    //    await stepContext.EndDialogAsync(null, cancellationToken);
                    //    return await stepContext.BeginDialogAsync(nameof(ViewAllFlightsDialog), null, cancellationToken);
                    //case "viewflights":
                    //    await stepContext.EndDialogAsync(null, cancellationToken);
                    //    return await stepContext.BeginDialogAsync(nameof(ViewFlightsDialog), null, cancellationToken);
                    //case "webcheckin":
                    //    await stepContext.EndDialogAsync(null, cancellationToken);
                    //    return await stepContext.BeginDialogAsync(nameof(WebCheckInDialog), null, cancellationToken);
                    //case "welcomeairport":
                    //    await stepContext.EndDialogAsync(null, cancellationToken);
                    //    return await stepContext.BeginDialogAsync(nameof(MainDialog), null, cancellationToken);
                    case "contactus":
                        await stepContext.PromptAsync(nameof(TextPrompt), new PromptOptions { Prompt = MessageFactory.Text($"You may contact us, please visit this link for all contact details https://www.newdelhiairport.in/contactus. We'd be happy to assist you in any way \U0001F600") }, cancellationToken);
                        await stepContext.EndDialogAsync(null, cancellationToken);
                        return await stepContext.BeginDialogAsync(nameof(AskMoreQuestionsDialog), null, cancellationToken);
                    case "knowledge.airportfaqterminals":
                        await stepContext.EndDialogAsync(null, cancellationToken);
                        return await stepContext.BeginDialogAsync(nameof(FAQDialog), null, cancellationToken);
                    default:
                        await stepContext.PromptAsync(nameof(TextPrompt), new PromptOptions { Prompt = MessageFactory.Text($"Sorry, I don’t understand {Environment.NewLine} You may ask me anything regarding the airport facilities, procedures or details about contacting us.") }, cancellationToken);
                        await stepContext.EndDialogAsync(null, cancellationToken);
                        return await stepContext.BeginDialogAsync(nameof(AskMoreQuestionsDialog), null, cancellationToken);
                }
            }
            else
            {
                await stepContext.PromptAsync(nameof(TextPrompt), new PromptOptions { Prompt = MessageFactory.Text($"Sorry, I don’t understand {Environment.NewLine} You may ask me anything regarding the airport facilities, procedures or details about contacting us.") }, cancellationToken);
                await stepContext.EndDialogAsync(null, cancellationToken);
                return await stepContext.BeginDialogAsync(nameof(AskMoreQuestionsDialog), null, cancellationToken);
            }
        }
    }
}
