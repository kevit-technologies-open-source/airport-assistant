﻿using AirportBot.Dialog;
using Microsoft.Bot.Builder;
using Microsoft.Bot.Builder.AI.Luis;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Builder.Dialogs.Choices;
using Microsoft.Bot.Schema;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace AirPortBot.Dialog
{
    public class ViewFlightsDialog : ComponentDialog
    {
        private LuisRecognizer Recognizer { get; } = null;

        public ViewFlightsDialog(LuisRecognizer luis)
              : base(nameof(ViewFlightsDialog))
        {
            this.Recognizer = luis ?? throw new System.ArgumentNullException(nameof(luis));
            AddDialog(new WaterfallDialog(nameof(WaterfallDialog), new WaterfallStep[]
            {
                ViewFlightsStepAsync,
                SelectedViewFlightOptionStepAsync,
            }));

            AddDialog(new ChoicePrompt(nameof(ChoicePrompt)));
            AddDialog(new TextPrompt(nameof(TextPrompt)));
            AddDialog(new TrackMyflightDialog(luis));
            AddDialog(new ViewAllFlightsDialog(luis));
            //AddDialog(new LUISDefaultDialog(luis));

            InitialDialogId = nameof(WaterfallDialog);
        }

        private static async Task<DialogTurnResult> ViewFlightsStepAsync(WaterfallStepContext stepContext, CancellationToken cancellationToken)
        {
            return await stepContext.PromptAsync(nameof(ChoicePrompt),

            new PromptOptions
            {
                Prompt = MessageFactory.Text("Of course, just let me know, which flights would you like to view"),
                RetryPrompt = MessageFactory.Text("That was not a valid choice, please select from given options."),
                Choices = new List<Choice>
                {
                    new Choice
                    {
                        Value = "Track My Flight",
                        Synonyms = new List<string>
                        {
                            "My Flights",
                            "Track My Flight",
                            "Track",
                            "track",
                            "Track Flights",
                        },
                    },
                    new Choice
                    {
                        Value = "View All",
                        Synonyms = new List<string>
                        {
                            "View All",
                            "All Flights",
                        },
                    }
                }
            }, cancellationToken);
        }

        private async Task<DialogTurnResult> SelectedViewFlightOptionStepAsync(WaterfallStepContext stepContext, CancellationToken cancellationToken)
        {
            var recognizerResult = await Recognizer.RecognizeAsync(stepContext.Context, cancellationToken);
            var topIntent = recognizerResult?.GetTopScoringIntent();
            string strIntent = (topIntent != null) ? topIntent.Value.intent : "";
            if (strIntent != "")
            {
                switch (strIntent.ToLower())
                {
                    case "none":
                        await stepContext.EndDialogAsync(null, cancellationToken);
                        return await stepContext.BeginDialogAsync(nameof(LUISDefaultDialog), null, cancellationToken);
                    case "viewflights":
                        await stepContext.EndDialogAsync(null, cancellationToken);
                        return await stepContext.BeginDialogAsync(nameof(ViewFlightsDialog), null, cancellationToken);
                    case "webcheckin":
                        await stepContext.EndDialogAsync(null, cancellationToken);
                        return await stepContext.BeginDialogAsync(nameof(WebCheckInDialog), null, cancellationToken);
                    case "airporttour":
                        await stepContext.EndDialogAsync(null, cancellationToken);
                        return await stepContext.BeginDialogAsync(nameof(AirportTourDialog), null, cancellationToken);
                    case "faq":
                        await stepContext.EndDialogAsync(null, cancellationToken);
                        return await stepContext.BeginDialogAsync(nameof(FAQDialog), null, cancellationToken);
                    case "trackmyflight":
                        await stepContext.EndDialogAsync(null, cancellationToken);
                        return await stepContext.BeginDialogAsync(nameof(TrackMyflightDialog), null, cancellationToken);
                    case "viewall":
                        await stepContext.EndDialogAsync(null, cancellationToken);
                        return await stepContext.BeginDialogAsync(nameof(ViewAllFlightsDialog), null, cancellationToken);
                    case "contactus":
                        await stepContext.EndDialogAsync(null, cancellationToken);
                        return await stepContext.PromptAsync(nameof(TextPrompt), new PromptOptions { Prompt = MessageFactory.Text($"You may contact us, please visit this link for all contact details https://www.newdelhiairport.in/contactus. We'd be happy to assist you in any way \U0001F600 {Environment.NewLine} Please contact us with details listed here https://www.newdelhiairport.in/contactus. We'd be glad to be of service.") }, cancellationToken);
                    case "negativeans":
                        await stepContext.EndDialogAsync(null, cancellationToken);
                        return await stepContext.BeginDialogAsync(nameof(DefaultMainDialog), null, cancellationToken);
                    default:
                        return await stepContext.PromptAsync(nameof(TextPrompt), new PromptOptions { Prompt = MessageFactory.Text("Sorry, I don’t understand") }, cancellationToken);
                }
            }
            return await stepContext.PromptAsync(nameof(TextPrompt), new PromptOptions { Prompt = MessageFactory.Text("Sorry, I don’t understand") }, cancellationToken);
        }
    }
}
