﻿// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT License.

using AirportBot.Dialog;
using AirPortBot.Dialog;
using Microsoft.Bot.Builder;
using Microsoft.Bot.Builder.AI.Luis;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Builder.Dialogs.Choices;
using Microsoft.Bot.Schema;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace AirPortBot
{
    public class MainDialog : ComponentDialog
    {
        //private readonly UserState _userState;
        private LuisRecognizer Recognizer { get; } = null;

        public MainDialog(LuisRecognizer luis, ConversationState conversationState, UserState userState)
            : base(nameof(MainDialog))
        {
            this.Recognizer = luis ?? throw new System.ArgumentNullException(nameof(luis));
            AddDialog(new WaterfallDialog(nameof(WaterfallDialog), new WaterfallStep[]
            {
                SelectedOptionStepAsync
        }));

            AddDialog(new TextPrompt(nameof(TextPrompt)));
            AddDialog(new ChoicePrompt(nameof(ChoicePrompt)));
            AddDialog(new ViewFlightsDialog(luis));
            AddDialog(new WebCheckInDialog(luis));
            AddDialog(new AirportTourDialog(luis));
            AddDialog(new FAQDialog(luis));
            AddDialog(new TrackMyflightDialog(luis));
            AddDialog(new ViewAllFlightsDialog(luis));
            AddDialog(new DefaultMainDialog(luis));
            AddDialog(new LUISDefaultDialog(luis));
            AddDialog(new DefaultDialog(luis));

            InitialDialogId = nameof(WaterfallDialog);
        }

        private async Task<DialogTurnResult> SelectedOptionStepAsync(WaterfallStepContext stepContext, CancellationToken cancellationToken)
        {
            if (stepContext.Context.Activity.Text.Equals("Yes Terminal 3"))
            {
                await stepContext.Context.SendActivityAsync(MessageFactory.Text($"That's great! T3 is our largest terminal with stunning amenities and option,{Environment.NewLine} Explore T3 and enjoy your stay at our airport"), cancellationToken: cancellationToken);
                var attachments = new List<Attachment>();
                var reply = MessageFactory.Attachment(attachments);
                reply.Attachments.Add(Common.CreateAdaptiveCardAttachment(Path.Combine(".", "Resources", "T3Map.json")));
                await stepContext.Context.SendActivityAsync(reply, cancellationToken);
                await stepContext.EndDialogAsync(null, cancellationToken);
                return await stepContext.BeginDialogAsync(nameof(DefaultDialog), null, cancellationToken);
            }
            else if (stepContext.Context.Activity.Text.Equals("No Terminal 3"))
            {
                await stepContext.EndDialogAsync(null, cancellationToken);
                return await stepContext.PromptAsync(nameof(TextPrompt), new PromptOptions { Prompt = MessageFactory.Text($"Goodbye traveller! Enjoy your flight and have a good day \U0001F600") }, cancellationToken);
                //return await stepContext.BeginDialogAsync(nameof(DefaultMainDialog), null, cancellationToken);
            }
            else if (stepContext.Context.Activity.Text.Contains("WebCheckFormSubmit"))
            {
                JToken commandToken = JToken.Parse(stepContext.Context.Activity.Value.ToString());
                string FullName = commandToken["FullName"].Value<string>();
                string DOB = commandToken["DOB"].Value<string>();
                string PassportNumber = commandToken["PassportNumber"].Value<string>();
                if (string.IsNullOrEmpty(FullName) || string.IsNullOrEmpty(DOB) || string.IsNullOrEmpty(PassportNumber))
                {
                    await stepContext.EndDialogAsync(null, cancellationToken);
                    return await stepContext.PromptAsync(nameof(TextPrompt), new PromptOptions { Prompt = MessageFactory.Text($"Please fill all fields") }, cancellationToken);
                }
            }
            else
            {
                var recognizerResult = await Recognizer.RecognizeAsync(stepContext.Context, cancellationToken);
                var topIntent = recognizerResult?.GetTopScoringIntent();
                string strIntent = (topIntent != null) ? topIntent.Value.intent : "";
                double dblIntentScore = (topIntent != null) ? topIntent.Value.score : 0.0;
                if (strIntent != "" && dblIntentScore >= 0.09)
                {
                    switch (strIntent.ToLower())
                    {
                        case "none":
                            await stepContext.EndDialogAsync(null, cancellationToken);
                            return await stepContext.BeginDialogAsync(nameof(LUISDefaultDialog), null, cancellationToken);
                        case "viewflights":
                            await stepContext.EndDialogAsync(null, cancellationToken);
                            return await stepContext.BeginDialogAsync(nameof(ViewFlightsDialog), null, cancellationToken);
                        case "webcheckin":
                            await stepContext.EndDialogAsync(null, cancellationToken);
                            return await stepContext.BeginDialogAsync(nameof(WebCheckInDialog), null, cancellationToken);
                        case "airporttour":
                            await stepContext.EndDialogAsync(null, cancellationToken);
                            return await stepContext.BeginDialogAsync(nameof(AirportTourDialog), null, cancellationToken);
                        case "faq":
                            await stepContext.EndDialogAsync(null, cancellationToken);
                            return await stepContext.BeginDialogAsync(nameof(FAQDialog), null, cancellationToken);
                        case "trackmyflight":
                            await stepContext.EndDialogAsync(null, cancellationToken);
                            return await stepContext.BeginDialogAsync(nameof(TrackMyflightDialog), null, cancellationToken);
                        case "viewall":
                            await stepContext.EndDialogAsync(null, cancellationToken);
                            return await stepContext.BeginDialogAsync(nameof(ViewAllFlightsDialog), null, cancellationToken);
                        case "contactus":
                            await stepContext.EndDialogAsync(null, cancellationToken);
                            await stepContext.PromptAsync(nameof(TextPrompt), new PromptOptions { Prompt = MessageFactory.Text($"You may contact us, please visit this link for all contact details https://www.newdelhiairport.in/contactus. We'd be happy to assist you in any way \U0001F600 {Environment.NewLine} Please contact us with details listed here https://www.newdelhiairport.in/contactus. We'd be glad to be of service.") }, cancellationToken);
                            return await stepContext.BeginDialogAsync(nameof(DefaultMainDialog), null, cancellationToken);
                        case "default_welcome_intent":
                            await stepContext.EndDialogAsync(null, cancellationToken);
                            return await stepContext.BeginDialogAsync(nameof(DefaultMainDialog), null, cancellationToken);
                        case "positiveans":
                            await stepContext.EndDialogAsync(null, cancellationToken);
                            return await stepContext.BeginDialogAsync(nameof(DefaultMainDialog), null, cancellationToken);
                        case "negativeans":
                            await stepContext.PromptAsync(nameof(TextPrompt), new PromptOptions { Prompt = MessageFactory.Text($"Goodbye traveller! Enjoy your flight and have a good day \U0001F600") }, cancellationToken);
                            await stepContext.EndDialogAsync(null, cancellationToken);
                            return await stepContext.BeginDialogAsync(nameof(DefaultMainDialog), null, cancellationToken);
                        case "greeting":
                            await stepContext.EndDialogAsync(null, cancellationToken);
                            return await stepContext.BeginDialogAsync(nameof(DefaultMainDialog), null, cancellationToken);
                        default:
                            await stepContext.EndDialogAsync(null, cancellationToken);
                            return await stepContext.BeginDialogAsync(nameof(LUISDefaultDialog), null, cancellationToken);
                    }
                }
            }
           return await stepContext.EndDialogAsync(null, cancellationToken);
            //return await stepContext.BeginDialogAsync(nameof(LUISDefaultDialog), null, cancellationToken);
        }
    }
}
