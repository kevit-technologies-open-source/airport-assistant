﻿using AirportBot.Dialog;
using Microsoft.Bot.Builder;
using Microsoft.Bot.Builder.AI.Luis;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Builder.Dialogs.Choices;
using Microsoft.Bot.Schema;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace AirPortBot.Dialog
{
    public class AirportTourDialog : ComponentDialog
    {
        private LuisRecognizer Recognizer { get; } = null;

        public AirportTourDialog(LuisRecognizer luis)
             : base(nameof(AirportTourDialog))
        {
            this.Recognizer = luis ?? throw new System.ArgumentNullException(nameof(luis));
            AddDialog(new ChoicePrompt(nameof(ChoicePrompt)));

            AddDialog(new WaterfallDialog(nameof(WaterfallDialog), new WaterfallStep[]
            {
                AirportTourStepAsync,
                GuideToTerminalStepAsync,
                SelectedTerminalStepAsync
            }));
            AddDialog(new DefaultDialog(luis));
           // AddDialog(new LUISDefaultDialog(luis));

            InitialDialogId = nameof(WaterfallDialog);
        }

        private static async Task<DialogTurnResult> AirportTourStepAsync(WaterfallStepContext stepContext, CancellationToken cancellationToken)
        {
            return await stepContext.PromptAsync(nameof(ChoicePrompt),
            new PromptOptions
            {
                Prompt = MessageFactory.Text($"So good to see you visitor! Here is your virtual guide to the airport so you can find everything you need!{Environment.NewLine} Here are some of the things you can explore with me"),
                RetryPrompt = MessageFactory.Text("That was not a valid choice, please select from given option."),
                Choices = new List<Choice>
                   {
                       new Choice
                       {
                            Value = "Guide to Terminals",
                            Synonyms = new List<string>
                            {
                                "Guide to Terminals",
                                "Terminals",
                                "Guide",
                                "Guide To",
                                "Airport Terminals",
                            },
                       }
                   }
            }, cancellationToken);
        }

        private static async Task<DialogTurnResult> GuideToTerminalStepAsync(WaterfallStepContext stepContext, CancellationToken cancellationToken)
        {
            return await stepContext.PromptAsync(nameof(ChoicePrompt),
            new PromptOptions
            {
                Prompt = MessageFactory.Text($"Great! So let’s start with helping you select a Terminal. You would like a guide to around which terminal?",InputHints.ExpectingInput),
                RetryPrompt = MessageFactory.Text("That was not a valid choice, please select from given option."),
                Choices = GetTerminalChoices(),
            }, cancellationToken);
        }

        private async Task<DialogTurnResult> SelectedTerminalStepAsync(WaterfallStepContext stepContext, CancellationToken cancellationToken)
        {
            var SelectedTerminal = ((FoundChoice)stepContext.Result).Value;
            if (SelectedTerminal.Equals("T1"))
            {
                await stepContext.Context.SendActivityAsync(MessageFactory.Text($"Oh sure!Explore our domestic terminal with this interactive map guide"), cancellationToken: cancellationToken);
                var attachments = new List<Attachment>();
                var reply = MessageFactory.Attachment(attachments);
                reply.Attachments.Add(Common.CreateAdaptiveCardAttachment(Path.Combine(".", "Resources", "T1Map.json")));
                await stepContext.Context.SendActivityAsync(reply, cancellationToken);
                await stepContext.EndDialogAsync(null, cancellationToken);
                return await stepContext.BeginDialogAsync(nameof(DefaultDialog), null, cancellationToken);
            }
            else if (SelectedTerminal.Equals("T2"))
            {
                await stepContext.Context.SendActivityAsync(MessageFactory.Text($"Awesome, let me take you around terminal 2 that comes right before departure area!  {Environment.NewLine} It has amazing amenities and options for shopping and dining"), cancellationToken: cancellationToken);
                var attachments = new List<Attachment>();
                var reply = MessageFactory.Attachment(attachments);
                reply.Attachments.Add(Common.CreateAdaptiveCardAttachment(Path.Combine(".", "Resources", "T2Map.json")));
                await stepContext.Context.SendActivityAsync(reply, cancellationToken);

                await stepContext.EndDialogAsync(null, cancellationToken);
                return await stepContext.BeginDialogAsync(nameof(DefaultDialog), null, cancellationToken);
            }
            else if (SelectedTerminal.Equals("T3"))
            {
                await stepContext.Context.SendActivityAsync(MessageFactory.Text($"That's great! T3 is our largest terminal with stunning amenities and option,{Environment.NewLine} Explore T3 and enjoy your stay at our airport"), cancellationToken: cancellationToken);

                var attachments = new List<Attachment>();
                var reply = MessageFactory.Attachment(attachments);
                reply.Attachments.Add(Common.CreateAdaptiveCardAttachment(Path.Combine(".", "Resources", "T3Map.json")));
                await stepContext.Context.SendActivityAsync(reply, cancellationToken);
                await stepContext.EndDialogAsync(null, cancellationToken);
                return await stepContext.BeginDialogAsync(nameof(DefaultDialog), null, cancellationToken);
            }
            else
            {
                var recognizerResult = await Recognizer.RecognizeAsync(stepContext.Context, cancellationToken);
                var topIntent = recognizerResult?.GetTopScoringIntent();
                string strIntent = (topIntent != null) ? topIntent.Value.intent : "";
                if (strIntent != "")
                {
                    switch (strIntent.ToLower())
                    {
                        case "none":
                            await stepContext.EndDialogAsync(null, cancellationToken);
                            return await stepContext.BeginDialogAsync(nameof(LUISDefaultDialog), null, cancellationToken);
                        case "viewflights":
                            await stepContext.EndDialogAsync(null, cancellationToken);
                            return await stepContext.BeginDialogAsync(nameof(ViewFlightsDialog), null, cancellationToken);
                        case "webcheckin":
                            await stepContext.EndDialogAsync(null, cancellationToken);
                            return await stepContext.BeginDialogAsync(nameof(WebCheckInDialog), null, cancellationToken);
                        case "airporttour":
                            await stepContext.EndDialogAsync(null, cancellationToken);
                            return await stepContext.BeginDialogAsync(nameof(AirportTourDialog), null, cancellationToken);
                        case "faq":
                            await stepContext.EndDialogAsync(null, cancellationToken);
                            return await stepContext.BeginDialogAsync(nameof(FAQDialog), null, cancellationToken);
                        case "trackmyflight":
                            await stepContext.EndDialogAsync(null, cancellationToken);
                            return await stepContext.BeginDialogAsync(nameof(TrackMyflightDialog), null, cancellationToken);
                        case "viewall":
                            await stepContext.EndDialogAsync(null, cancellationToken);
                            return await stepContext.BeginDialogAsync(nameof(ViewAllFlightsDialog), null, cancellationToken);
                        case "contactus":
                            await stepContext.EndDialogAsync(null, cancellationToken);
                            return await stepContext.PromptAsync(nameof(TextPrompt), new PromptOptions { Prompt = MessageFactory.Text($"You may contact us, please visit this link for all contact details https://www.newdelhiairport.in/contactus. We'd be happy to assist you in any way \U0001F600 {Environment.NewLine} Please contact us with details listed here https://www.newdelhiairport.in/contactus. We'd be glad to be of service.") }, cancellationToken);
                        default:
                            return await stepContext.PromptAsync(nameof(TextPrompt), new PromptOptions { Prompt = MessageFactory.Text("Sorry, I don’t understand") }, cancellationToken);
                    }
                }
                return await stepContext.PromptAsync(nameof(TextPrompt), new PromptOptions { Prompt = MessageFactory.Text("Sorry, I don’t understand") }, cancellationToken);
            }
        }

        private static IList<Choice> GetTerminalChoices()
        {
            var cardOptions = new List<Choice>()
            {
                new Choice() { Value = "T1", Synonyms = new List<string>() { "Terminal 1" } },
                new Choice() { Value = "T2", Synonyms = new List<string>() { "Terminal 2" } },
                new Choice() { Value = "T3", Synonyms = new List<string>() { "Terminal 3" } },
            };
            return cardOptions;
        }
    }
}
