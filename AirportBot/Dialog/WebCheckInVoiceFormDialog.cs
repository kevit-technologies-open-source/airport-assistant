﻿using Microsoft.Bot.Builder;
using Microsoft.Bot.Builder.AI.Luis;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Schema;
using Microsoft.Recognizers.Text.DataTypes.TimexExpression;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace AirPortBot.Dialog
{
    public class WebCheckInVoiceFormDialog : ComponentDialog
    {
        private LuisRecognizer Recognizer { get; } = null;

        public WebCheckInVoiceFormDialog(LuisRecognizer luis)
           : base(nameof(WebCheckInVoiceFormDialog))
        {
            AddDialog(new WaterfallDialog(nameof(WaterfallDialog), new WaterfallStep[]
           {
                 AskUserCheckInDetail,
                AskUserCheckInDetailMore,
                CheckForAirportType,
           }));

            AddDialog(new TextPrompt(nameof(TextPrompt)));
            AddDialog(new ChoicePrompt(nameof(ChoicePrompt)));
            AddDialog(new WebcheckInDomestic(luis));
            AddDialog(new WebCheckInInternational(luis));
            AddDialog(new DefaultDialog(luis));

            InitialDialogId = nameof(WaterfallDialog);
        }
        private static async Task<DialogTurnResult> AskUserCheckInDetail(WaterfallStepContext stepContext, CancellationToken cancellationToken)
        {
            return await stepContext.PromptAsync(nameof(TextPrompt), new PromptOptions { Prompt = MessageFactory.Text($"please tell me your full name?") }, cancellationToken);
        }

        private static async Task<DialogTurnResult> AskUserCheckInDetailMore(WaterfallStepContext stepContext, CancellationToken cancellationToken)
        {
            stepContext.Values["FullName"] = stepContext.Result;
            return await stepContext.PromptAsync(nameof(TextPrompt), new PromptOptions { Prompt = MessageFactory.Text($"And what is your Date of Birth?", InputHints.ExpectingInput) }, cancellationToken);
        }

        private static async Task<DialogTurnResult> CheckForAirportType(WaterfallStepContext stepContext, CancellationToken cancellationToken)
        {
            stepContext.Values["DOB"] = stepContext.Result;
            await stepContext.EndDialogAsync(null, cancellationToken);
            return await stepContext.BeginDialogAsync(nameof(WebCheckInInternational), null, cancellationToken);
        }
    }
}
