﻿using Microsoft.Bot.Builder;
using Microsoft.Bot.Builder.AI.Luis;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Builder.Dialogs.Choices;
using Microsoft.Bot.Schema;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace AirPortBot.Dialog
{
    public class WebCheckInInternational : ComponentDialog
    {
        private LuisRecognizer Recognizer { get; } = null;

        public WebCheckInInternational(LuisRecognizer luis)
         : base(nameof(WebCheckInInternational))
        {
            this.Recognizer = luis ?? throw new System.ArgumentNullException(nameof(luis));

            AddDialog(new WaterfallDialog(nameof(WaterfallDialog), new WaterfallStep[]
           {
               InternationalCheckIn,
               InternationalTerminalGuide,
               InternationalTerminalGuideStart
           }));

            AddDialog(new TextPrompt(nameof(TextPrompt)));
            AddDialog(new ChoicePrompt(nameof(ChoicePrompt)));
            AddDialog(new DefaultDialog(luis));

            InitialDialogId = nameof(WaterfallDialog);
        }

        private static async Task<DialogTurnResult> InternationalCheckIn(WaterfallStepContext stepContext, CancellationToken cancellationToken)
        {
            return await stepContext.PromptAsync(nameof(TextPrompt), new PromptOptions { Prompt = MessageFactory.Text($"Interesting, looks like you're flying international, please tell me your passport number") }, cancellationToken);
        }

        private static async Task<DialogTurnResult> InternationalTerminalGuide(WaterfallStepContext stepContext, CancellationToken cancellationToken)
        {
            stepContext.Values["PasswordNumber"] = stepContext.Result;
            if (stepContext.Result != null)
            {
                await stepContext.Context.SendActivityAsync(MessageFactory.Text($"That's all! Your web check in has been successful! \U0001F600 {Environment.NewLine}Looks like we're done, here's a tip, since you're flying international expect your flight to arrive at the terminal 3"), cancellationToken);
            }
            else
            {
                await stepContext.PromptAsync(nameof(TextPrompt), new PromptOptions { Prompt = MessageFactory.Text($"Interesting, looks like you're flying international, please tell me your passport number") }, cancellationToken);
            }
            return await stepContext.PromptAsync(nameof(ChoicePrompt),
            new PromptOptions
            {
                Prompt = MessageFactory.Text("Would you like to view a guide to T3?", InputHints.ExpectingInput),
                RetryPrompt = MessageFactory.Text("That was not a valid choice, please select from given options."),
                Choices = ChoiceFactory.ToChoices(new List<string> { "Yes", "No" }),
            }, cancellationToken);
        }

        private static async Task<DialogTurnResult> InternationalTerminalGuideStart(WaterfallStepContext stepContext, CancellationToken cancellationToken)
        {
            var SelectedInternationalResult = ((FoundChoice)stepContext.Result).Value;
            if (SelectedInternationalResult.ToLower().Equals("yes"))
            {
                var attachments = new List<Attachment>();
                var reply = MessageFactory.Attachment(attachments);
                reply.Attachments.Add(Common.CreateAdaptiveCardAttachment(Path.Combine(".", "Resources", "T3Map.json")));
                await stepContext.Context.SendActivityAsync(reply, cancellationToken);
                await stepContext.EndDialogAsync(null, cancellationToken);
                return await stepContext.BeginDialogAsync(nameof(DefaultDialog), null, cancellationToken);
            }
            else
            {
                await stepContext.EndDialogAsync(null, cancellationToken);
                return await stepContext.BeginDialogAsync(nameof(DefaultDialog), null, cancellationToken);
            }
        }
    }
}
