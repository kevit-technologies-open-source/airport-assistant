﻿using Microsoft.Bot.Builder;
using Microsoft.Bot.Builder.AI.Luis;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Schema;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace AirPortBot.Dialog
{
    public class WebCheckInFormDialog : ComponentDialog
    {
        private LuisRecognizer Recognizer { get; } = null;

        public WebCheckInFormDialog(LuisRecognizer luis)
            : base(nameof(WebCheckInFormDialog))
        {
            AddDialog(new WaterfallDialog(nameof(WaterfallDialog), new WaterfallStep[]
           {
                DisplayWenCheckINForm,
           }));

            AddDialog(new TextPrompt(nameof(TextPrompt)));
            AddDialog(new ChoicePrompt(nameof(ChoicePrompt)));
            AddDialog(new DefaultDialog(luis));

            InitialDialogId = nameof(WaterfallDialog);
        }

        private static async Task<DialogTurnResult> DisplayWenCheckINForm(WaterfallStepContext stepContext, CancellationToken cancellationToken)
        {
            var attachments = new List<Attachment>();
            var reply = MessageFactory.Attachment(attachments);
            reply.Attachments.Add(Common.CreateAdaptiveCardAttachment(Path.Combine(".", "Resources", "JourneyDetailForm.json")));
            await stepContext.Context.SendActivityAsync(reply, cancellationToken);
            return await stepContext.PromptAsync(nameof(TextPrompt), new PromptOptions { Prompt = MessageFactory.Text($"Press button to submit information") }, cancellationToken);
        }

        private static async Task<DialogTurnResult> SubmitedWebCheckInFormDetail(WaterfallStepContext stepContext, CancellationToken cancellationToken)
        {
            var activity = stepContext.Context.Activity;
            if (string.IsNullOrWhiteSpace(activity.Text) && activity.Value != null)
            {
                activity.Text = JsonConvert.SerializeObject(activity.Value);
            }

            var attachments = new List<Attachment>();
            var reply = MessageFactory.Attachment(attachments);
            reply.Attachments.Add(Common.CreateAdaptiveCardAttachment(Path.Combine(".", "Resources", "JourneyDetailForm.json")));
            await stepContext.Context.SendActivityAsync(reply, cancellationToken);
            await stepContext.EndDialogAsync(stepContext, cancellationToken);
            return await stepContext.PromptAsync(nameof(TextPrompt), new PromptOptions { Prompt = MessageFactory.Text($"Press button to submit information") }, cancellationToken);
        }
    }
}
