﻿using Microsoft.Bot.Builder;
using Microsoft.Bot.Builder.AI.Luis;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Schema;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace AirPortBot.Dialog
{
    public class WebcheckInSubmitFormDialog : ComponentDialog
    {
        private LuisRecognizer Recognizer { get; } = null;

        public WebcheckInSubmitFormDialog(LuisRecognizer luis)
          : base(nameof(WebcheckInSubmitFormDialog))
        {
            AddDialog(new WaterfallDialog(nameof(WaterfallDialog), new WaterfallStep[]
           {
                AskPNRNumberStepAsync,
           }));

            AddDialog(new TextPrompt(nameof(TextPrompt)));
            AddDialog(new ChoicePrompt(nameof(ChoicePrompt)));
            AddDialog(new DefaultDialog(luis));

            InitialDialogId = nameof(WaterfallDialog);
        }

        private static async Task<DialogTurnResult> AskPNRNumberStepAsync(WaterfallStepContext stepContext, CancellationToken cancellationToken)
        {
            return await stepContext.PromptAsync(nameof(TextPrompt), new PromptOptions { Prompt = MessageFactory.Text($"Of course! I see that you are ready to check in, please enter your PNR number and I'll make this quick for you") }, cancellationToken);
        }
    }
}
