﻿using Microsoft.Bot.Builder;
using Microsoft.Bot.Builder.AI.Luis;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Builder.Dialogs.Choices;
using Microsoft.Bot.Schema;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace AirPortBot.Dialog
{
    public class WebcheckInDomestic : ComponentDialog
    {
        private LuisRecognizer Recognizer { get; } = null;

        public WebcheckInDomestic(LuisRecognizer luis)
         : base(nameof(WebcheckInDomestic))
        {
            AddDialog(new WaterfallDialog(nameof(WaterfallDialog), new WaterfallStep[]
           {
               DomesticCheckIn,
               DomesticTerminalGuide,
           }));

            AddDialog(new TextPrompt(nameof(TextPrompt)));
            AddDialog(new ChoicePrompt(nameof(ChoicePrompt)));
            AddDialog(new DefaultDialog(luis));

            InitialDialogId = nameof(WaterfallDialog);
        }

        private static async Task<DialogTurnResult> DomesticCheckIn(WaterfallStepContext stepContext, CancellationToken cancellationToken)
        {
            await stepContext.PromptAsync(nameof(TextPrompt), new PromptOptions { Prompt = MessageFactory.Text($"That's all then! Your Web Check in has been successful, enjoy your flight \U0001F600 {Environment.NewLine} Herre's a tip \U0001F600 since you have a domestic flight it will be arriving at Terminal 1") }, cancellationToken);

            return await stepContext.PromptAsync(nameof(ChoicePrompt),
            new PromptOptions
            {
                Prompt = MessageFactory.Text("Would you like to view a guide to T1?"),
                RetryPrompt = MessageFactory.Text("That was not a valid choice, please select from given options."),
                Choices = ChoiceFactory.ToChoices(new List<string> { "Yes", "No" }),
            }, cancellationToken);
        }

        private static async Task<DialogTurnResult> DomesticTerminalGuide(WaterfallStepContext stepContext, CancellationToken cancellationToken)
        {
            var SelectedDomesticResult = ((FoundChoice)stepContext.Result).Value;
            if (SelectedDomesticResult.Equals("Yes"))
            {
                var attachments = new List<Attachment>();
                var reply = MessageFactory.Attachment(attachments);
                reply.Attachments.Add(Common.CreateAdaptiveCardAttachment(Path.Combine(".", "Resources", "T1Map.json")));
                await stepContext.Context.SendActivityAsync(reply, cancellationToken);
                return await stepContext.PromptAsync(nameof(TextPrompt), new PromptOptions { Prompt = MessageFactory.Text($"Map of Terminal 1") }, cancellationToken);
            }
            else
            {
                return await stepContext.BeginDialogAsync(nameof(DefaultDialog), null, cancellationToken);
            }
        }
    }
}
