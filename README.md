﻿# Airport Assistant

## 1. Overview

The project uses conversational design to help the Airport Passengers to navigate through airport and guide & give assistance to airport related tasks. The POC contains  the assistant which can do following tasks:

- View Flights
- Web Check In
- Airport Tour
- FAQs

The assistant can be extended to handle as many tasks as required and can be connected to any 3rd party system via application programming interface (API) to fetch the data from airport system or to push data into airport system. 

## 2. Architecture

![Airport bot - architecture](./imgs/Airport bot - architecture.png)

### 2.1 User interface

- User facing components through which user will be interacting. 
- Currently, it's a web based interface, but can be integrated on any platform and any size of devices. 

### 2.2 Azure bot channel registration

- Azure bot channel registration is used to route the messages from user interface to our bot logic. 
- This component can be replaced with custom routing solution.

### 2.3 Bot Components

- Bot components are the brains of the bot. They handle the bot logic. 
- Bot logic uses memory to store session. By storing sessions in memory, bot service is stateless and more scalable. 

### 2.4 Service integrations

- Bot logic uses service integrations to communicate with 3rd party services. 
- Bot can uses following service integrations:
  - Sky Scanner API: Cheaper Flights,Flights Comparison 
  - LUIS: NLU APIs to detect user intents. 

## 3. Configuration

### 3.1 Prerequisite

Some prior knowledge to following technologies:

- DotNet Core 2.1
- Microsoft Bot Framework
- Azure account

### 3.2 Import LUIS agent

- To import a LUIS app, log in to [www.luis.ai](http://www.luis.ai/), go to **My apps**, and click **Import App**. Choose the file JSON file for the app you want to import and click **Import**.
- The json file to restore app is in the source code with name `AirportBot - Final.json` 

### 3.3 Bot channel registration

- Check the guide on [Azure](https://docs.microsoft.com/en-us/azure/bot-service/bot-service-quickstart-registration?view=azure-bot-service-3.0) on how to create bot channel registration. 

- Please note that you would need **Microsoft App id** & **Microsoft App password**. 

- Put Microsoft App id in the file `appsettings.json` in: `MicrosoftAppId`

- Put Microsoft App password in the file `appsettings.json` in: `MicrosoftAppPassword`

  ![Screenshot Appsettings](./imgs/Screenshot Appsettings.PNG)

- Get the directline token follow [this tutorial](https://docs.microsoft.com/en-us/azure/bot-service/bot-service-channel-connect-directline?view=azure-bot-service-4.0). Note the **Directline Token**

### 4. Get up and running

- To setup the app, you will need to install DotNet 2.1 core on your machine. After that, follow the below steps:

  1. Clone the repository
  2. Install packages 

  ```bash
  $ dotnet restore	
  ```

  3. Build the app

  ```bash
  $ dotnet publish --configuration Release
  ```

  4. Put the directline token that you've previously aquired in file `AirpotBot/wwwroot/index.html` at line 238 as value of `token`.

  5. Start the app

  ```
  $ dotnet ./bin/Release/netcoreapp2.1/AirportBot.dll --urls "http://127.0.0.1:4800;https://127.0.0.1:4801"
  ```

  

- Alternatively, you can also run the project in Visual studio. To do so please follow below procedure:

  - Launch Visual Studio  - File -> Open -> Project/Solution
  - Navigate to AirportBot folder  - Select AirportBot.csproj file
  - Press F5 to run the project


